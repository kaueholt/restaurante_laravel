<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmbEmpresaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'AMB_EMPRESA';

    /**
     * Run the migrations.
     * @table AMB_EMPRESA
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('EMPRESA');
            $table->string('NOMEEMPRESA', 50)->nullable();
            $table->string('NOMEFANTASIA', 15)->nullable();
            $table->string('TIPOEND', 15)->nullable();
            $table->string('TITULOEND', 15)->nullable();
            $table->string('ENDERECO', 40)->nullable();
            $table->string('NUMERO', 7)->nullable();
            $table->string('COMPLEMENTO', 15)->nullable();
            $table->string('BAIRRO', 20)->nullable();
            $table->string('CIDADE', 40)->nullable();
            $table->string('UF', 2)->nullable();
            $table->integer('CEP')->nullable();
            $table->string('CNPJ', 14)->nullable();
            $table->string('INSCESTADUAL', 15)->nullable();
            $table->string('INSCMUNICIPAL', 20)->nullable();
            $table->string('FONEAREA', 4)->nullable();
            $table->string('FONENUM', 8)->nullable();
            $table->string('FAXAREA', 4)->nullable();
            $table->string('FAXNUM', 8)->nullable();
            $table->time('INICIOMANHA')->nullable()->default('00:00:00');
            $table->time('FIMMANHA')->nullable()->default('00:00:00');
            $table->time('INICIOTARDE')->nullable()->default('00:00:00');
            $table->time('FIMTARDE')->nullable()->default('00:00:00');
            $table->time('INICIOSABADO')->nullable()->default('00:00:00');
            $table->time('FIMSABADO')->nullable()->default('00:00:00');
            $table->string('FAX2AREA', 4)->nullable();
            $table->string('FAX2NUM', 8)->nullable();
            $table->string('EMAIL', 50)->nullable();
            $table->integer('CENTROCUSTO')->nullable();
            $table->integer('CLIENTE')->nullable();
            $table->integer('FORNECEDOR')->nullable();
            $table->binary('LOGO')->nullable();
            $table->string('NOMECONTATO', 50)->nullable();
            $table->smallInteger('CONTA')->nullable();
            $table->string('CODMUNICIPIO', 10)->nullable();
            $table->char('REGIMETRIBUTARIO', 1)->nullable()->default('3')->comment('1=Simples Nacional; 2=Simples Nacional, excesso sublimite de receita bruta; 3=Regime Normal. (v2.0). ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
