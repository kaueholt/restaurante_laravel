<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstProdutoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'EST_PRODUTO';

    /**
     * Run the migrations.
     * @table EST_PRODUTO
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->string('DESCRICAO', 100)->nullable();
            $table->string('GRUPO', 15);
            $table->string('UNIDADE', 3)->nullable();
            $table->decimal('PRECOVENDA', 14, 2)->nullable();
            $table->text('OBS')->nullable();
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();

            $table->index(["GRUPO"], 'fk_est_produto_est_grupo1_idx');


            $table->foreign('GRUPO', 'fk_est_produto_est_grupo1_idx')
                ->references('GRUPO')->on('EST_GRUPO')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
