<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFatPedidoItemAdicionalTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'FAT_PEDIDO_ITEM_ADICIONAL';

    /**
     * Run the migrations.
     * @table FAT_PEDIDO_ITEM_ADICIONAL
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->unsignedInteger('ID_PEDIDO_ITEM');
            $table->unsignedInteger('ID_PRODUTO');
            $table->double('VALOR')->nullable();
            $table->double('QUANTIDADE')->nullable();
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();

            $table->index(["ID_PEDIDO_ITEM"], 'fk_fat_pedido_item_adicional_fat_pedido_item1_idx');

            $table->index(["ID_PRODUTO"], 'fk_fat_pedido_item_adicional_est_produto1_idx');


            $table->foreign('ID_PEDIDO_ITEM', 'fk_fat_pedido_item_adicional_fat_pedido_item1_idx')
                ->references('ID')->on('FAT_PEDIDO_ITEM')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('ID_PRODUTO', 'fk_fat_pedido_item_adicional_est_produto1_idx')
                ->references('ID')->on('EST_PRODUTO')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
