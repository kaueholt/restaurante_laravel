<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstGrupoAdicionalTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'EST_GRUPO_ADICIONAL';

    /**
     * Run the migrations.
     * @table EST_GRUPO_ADICIONAL
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->string('GRUPO', 15);
            $table->unsignedInteger('ID_PRODUTO');
            $table->unsignedInteger('ID_GRUPO_RESTAURANTE');
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();

            $table->index(["GRUPO"], 'fk_est_grupo_adicional_est_grupo1_idx');

            $table->index(["ID_GRUPO_RESTAURANTE"], 'fk_est_grupo_adicional_est_grupo_restaurante1_idx');

            $table->index(["ID_PRODUTO"], 'fk_est_grupo_adicional_est_produto1_idx');


            $table->foreign('GRUPO', 'fk_est_grupo_adicional_est_grupo1_idx')
                ->references('GRUPO')->on('EST_GRUPO')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('ID_PRODUTO', 'fk_est_grupo_adicional_est_produto1_idx')
                ->references('ID')->on('EST_PRODUTO')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('ID_GRUPO_RESTAURANTE', 'fk_est_grupo_adicional_est_grupo_restaurante1_idx')
                ->references('ID')->on('EST_GRUPO_RESTAURANTE')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
