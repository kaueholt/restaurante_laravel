<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmbUsuarioTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'AMB_USUARIO';

    /**
     * Run the migrations.
     * @table AMB_USUARIO
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('USUARIO');
            $table->string('NOME', 40);
            $table->string('LOGIN', 40);
            $table->string('NOMEREDUZIDO', 15);
            $table->integer('ID_PESSOA');
            $table->string('SENHA', 15);
            $table->char('SITUACAO', 1)->default('A');
            $table->char('TIPO', 1)->default('O');
            $table->integer('GRUPO')->nullable();
            $table->string('SMTP', 60)->nullable();
            $table->string('EMAIL', 60)->nullable();
            $table->string('EMAILSENHA', 15)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
