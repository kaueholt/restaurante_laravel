<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinJobStatusTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'FIN_JOB_STATUS';

    /**
     * Run the migrations.
     * @table FIN_JOB_STATUS
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->unsignedInteger('ID_JOB');
            $table->integer('VALORANTIGO')->nullable();
            $table->integer('VALORNOVO')->nullable();
            $table->timestamp('DATAALTERACAO')->nullable();
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();

            $table->index(["ID_JOB"], 'fk_fin_jobs_status_fat_pedido_item1_idx');


            $table->foreign('ID_JOB', 'fk_fin_jobs_status_fat_pedido_item1_idx')
                ->references('ID')->on('FAT_PEDIDO_ITEM')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
