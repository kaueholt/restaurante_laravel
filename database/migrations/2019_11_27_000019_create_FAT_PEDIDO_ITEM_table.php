<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFatPedidoItemTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'FAT_PEDIDO_ITEM';

    /**
     * Run the migrations.
     * @table FAT_PEDIDO_ITEM
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID')->comment('job');
            $table->unsignedInteger('PEDIDO_ID');
            $table->smallInteger('NRITEM')->nullable();
            $table->unsignedInteger('ID_PRODUTO_RESTAURANTE');
            $table->decimal('QTSOLICITADA', 9, 4)->nullable()->default('0.0000');
            $table->decimal('QTATENDIDA', 9, 4)->nullable()->default('0.0000');
            $table->date('ATENDIMENTO')->nullable();
            $table->decimal('VALORUNITARIO', 11, 4)->nullable()->default('0.0000');
            $table->decimal('VALORDESCONTO', 9, 2)->nullable()->default('0.00');
            $table->decimal('VALORTOTAL', 11, 2)->nullable();
            $table->text('DESCRICAO')->nullable();
            $table->integer('STATUS')->nullable();
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();

            $table->index(["ID_PRODUTO_RESTAURANTE"], 'fk_FAT_PEDIDO_ITEM_EST_PRODUTO_RESTAURANTE1_idx');

            $table->index(["PEDIDO_ID"], 'fk_fat_pedido_item_fat_pedido1_idx');


            $table->foreign('PEDIDO_ID', 'fk_fat_pedido_item_fat_pedido1_idx')
                ->references('ID')->on('FAT_PEDIDO')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('ID_PRODUTO_RESTAURANTE', 'fk_FAT_PEDIDO_ITEM_EST_PRODUTO_RESTAURANTE1_idx')
                ->references('ID')->on('EST_PRODUTO_RESTAURANTE')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
