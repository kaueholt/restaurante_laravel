<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstRestauranteMesaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'EST_RESTAURANTE_MESA';

    /**
     * Run the migrations.
     * @table EST_RESTAURANTE_MESA
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->unsignedInteger('ID_RESTAURANTE');
            $table->string('DESCRICAO', 45)->nullable();
            $table->string('LINKQRCODE', 200)->nullable();
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();

            $table->index(["ID_RESTAURANTE"], 'fk_est_restaurante_mesa_fin_cliente1_idx');


            $table->foreign('ID_RESTAURANTE', 'fk_est_restaurante_mesa_fin_cliente1_idx')
                ->references('ID')->on('CRM_PESSOA')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
