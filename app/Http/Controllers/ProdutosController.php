<?php

namespace App\Http\Controllers;

use App\Models\produtos;
use App\Models\restaurantesprodutos;
use Illuminate\Http\Request;
use App\Http\Controllers\Utils\Handles;
use Illuminate\Support\Facades\DB;

class ProdutosController extends Controller
{

    /**
     * @OA\Get(
     *     path="/api/v1/produtos/promocoes/geoloc/{latitude}/{longitude}/{cidade}",
     *     tags={"Custom"},
     *     summary="Lista produtos em promoção por latitude e longitude",
     *     description="Retorna todos os produtos em promoções por latitude e longitude",
     *     @OA\Parameter(
     *         name="cidade",
     *         in="path",
     *         required=true,
     *         description="pesquisa utilizando parametro LIKE % %",
     *         @OA\Schema(
     *             type="string",
     *             example="%CURITIBA%"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="latitude",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="-25.4735061"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="longitude",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="-49.2817559"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="cidade",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="Curitiba"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showProdutosPromocoesGeolocCidade( $latitude, $longitude, $cidade = null ){
        $sis_url = env('SISTEMA_URL');
        $pessoa = new PessoasController();

        $sql =  "SELECT R.ID_RESTAURANTE, P.DESCRICAO, C.NOME, C.CIDADE, C.BAIRRO, C.LATITUDE, C.LONGITUDE,
                    C.INICIOMANHA,
                    C.FIMMANHA,
                    C.INICIOTARDE,
                    C.FIMTARDE,
                    C.INICIOSABADOMANHA,
                    C.INICIOSABADOTARDE,
                    C.FIMSABADOMANHA,
                    C.FIMSABADOTARDE,
                    C.INICIODOMINGOMANHA,
                    C.INICIODOMINGOTARDE,
                    C.FIMDOMINGOMANHA,
                    C.FIMDOMINGOTARDE,
                    C.DIASEMANA,
                    R.OBS, R.PRECOVENDA, R.PRECOPROMOCAO, R.INICIOPROMOCAO, R.FIMPROMOCAO, R.ATIVO,
                    '' AS DISTANCIAGOOGLEMETROS,
                    '' AS DISTANCIAGOOGLETEXTUAL,
                    '' AS TEMPOESTIMADOGOOGLETEXTUAL,
                    '' AS TEMPOESTIMADOGOOGLESEGUNDOS,
                    '' AS DISTANCIAKMLINHARETA,
                    NULL AS ERROAPIGOOGLEMAPS,
                    '' AS DISTANCIAKM,
                    CAST($latitude AS DECIMAL(10,7)) AS CLILATITUDE,
                    CAST($longitude AS DECIMAL(10,7)) AS CLILONGITUDE, ";
        $sql .= "IF( (I.ID <> '' and I.ID is not null), CONCAT('".$sis_url."','/images/doc/',LOWER(I.MODULO),'/',I.ID_DOC,'/',I.ID,'.jpg'), '') as LINK, ";
        $sql .= "IF(FIND_IN_SET(WEEKDAY(CURDATE()), C.DIASEMANA) && ( ";
        $sql .= "(WEEKDAY(CURDATE()) < 5 && ((CURRENT_TIME() > C.INICIOMANHA && CURRENT_TIME() < C.FIMMANHA) || ";
        $sql .= "(CURRENT_TIME() > C.INICIOTARDE && CURRENT_TIME() < C.FIMTARDE))) || ";
        $sql .= "(WEEKDAY(CURDATE()) = 5 && ((CURRENT_TIME() > C.INICIOSABADOMANHA && CURRENT_TIME() < C.FIMSABADOMANHA) || ";
        $sql .= "(CURRENT_TIME() > C.INICIOSABADOTARDE && CURRENT_TIME() < C.FIMSABADOTARDE))) || ";
        $sql .= "(WEEKDAY(CURDATE()) = 6 && ((CURRENT_TIME() > C.INICIODOMINGOMANHA && CURRENT_TIME() < C.FIMDOMINGOMANHA) || ";
        $sql .= "(CURRENT_TIME() > C.INICIODOMINGOTARDE && CURRENT_TIME() < C.FIMDOMINGOTARDE)))),true,false) as ABERTO ";
        $sql .= "FROM EST_PRODUTO_RESTAURANTE R ";
        $sql .= "RIGHT JOIN EST_PRODUTO P ON (P.ID=R.ID_PRODUTO) ";
        $sql .= "RIGHT JOIN CRM_PESSOA C ON (C.ID=R.ID_RESTAURANTE) ";
        $sql .= "LEFT JOIN AMB_IMAGEM I ON R.ID=I.ID_DOC AND LOWER(I.MODULO) = 'est' ";
        $sql .= "WHERE R.ATIVO='S' AND (R.INICIOPROMOCAO <= CURDATE()) and (R.FIMPROMOCAO >= CURDATE()) AND PRECOPROMOCAO > 0 ";
        is_null($cidade) ? null : $sql .= "AND C.CIDADE LIKE '%". $cidade. "%' ";
        $sql .= "order by R.PRECOPROMOCAO";

        $response = DB::select($sql);
        if(!$response){
            return Handles::jsonResponse('false', 'Nenhuma promoção encontrada!',[]);
        }
        $destinos = '';
        $cliLatitude = strlen($response[0]->CLILATITUDE) ? (double) $response[0]->CLILATITUDE : null;
        $cliLongitude = strlen($response[0]->CLILONGITUDE) ? (double) $response[0]->CLILONGITUDE : null;
        $response = DB::select($sql);
        for($i=0; $i < sizeof($response); $i++){            
            if($response[$i]->ABERTO){
                $diaDaSemanaHoje = date('w');
                $horaMinutoAgora = date('H:i');
                $horaMinutoAgoraEmSegundosUnix = strtotime(date('H:i'));
                switch($diaDaSemanaHoje){
                    case 0: //domingo
                        if($horaMinutoAgora < $response[$i]->FIMDOMINGOMANHA)
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMDOMINGOMANHA))) - $horaMinutoAgoraEmSegundosUnix;
                        else
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMDOMINGOTARDE))) - $horaMinutoAgoraEmSegundosUnix;
                    break;
                    case 6: //sabado
                        if($horaMinutoAgora < $response[$i]->FIMSABADOMANHA)
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMSABADOMANHA))) - $horaMinutoAgoraEmSegundosUnix;
                        else
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMSABADOTARDE))) - $horaMinutoAgoraEmSegundosUnix;
                    break;
                    default:
                        if($horaMinutoAgora < $response[$i]->FIMMANHA)
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMMANHA))) - $horaMinutoAgoraEmSegundosUnix;
                        else
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMTARDE))) - $horaMinutoAgoraEmSegundosUnix;
                        
                    break;
                }
                $minutes = floor(($diffUnix % 3600) / 60);
                $hours = floor($diffUnix / 3600);
                $diff = date('H:i',strtotime($hours.':'.$minutes));
                $response[$i]->HORAMINUTOPARAFECHAR = $diff;
            }
            // $response[$i]->DISTANCIAKMLINHARETA = $this->calculaDistanciaAPartirDasLatitudesLongitudes($response[$i]->CLILATITUDE, $response[$i]->CLILONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE);
            $response[$i]->LATITUDE = strlen($response[$i]->LATITUDE) ? (double) $response[$i]->LATITUDE : null;
            $response[$i]->LONGITUDE = strlen($response[$i]->LONGITUDE) ? (double) $response[$i]->LONGITUDE : null;
            $response[$i]->CLILATITUDE = strlen($cliLatitude) ? (double) $cliLatitude : null;
            $response[$i]->CLILONGITUDE = strlen($cliLongitude) ? (double) $cliLongitude : null;
            $response[$i]->AVALIACAO = $pessoa->calculaRating($response[$i]->ID_RESTAURANTE);
            $response[$i]->AVALIACAO = isset($response[$i]->AVALIACAO[0]) ? isset($response[$i]->AVALIACAO[0]->RATE) ? $response[$i]->AVALIACAO[0]->RATE : null : null;
            $destinos .= $response[$i]->LATITUDE .','. $response[$i]->LONGITUDE .'|';
            unset($response[$i]->PASSWORD);
            unset($response[$i]->SENHA);
            unset($response[$i]->USERLOGIN);
            unset($response[$i]->EMAIL);
            unset($response[$i]->EMAILSENHA);
        }
        $destinos = strlen($destinos) && strlen($cliLatitude) && strlen($cliLongitude)
        ? substr($destinos, 0, -1)
        : null;
        $responseJsonFromGoogleMatrixAPI = strlen($destinos)
        ? $this->callGoogleMapsMatrixAPI($cliLatitude, $cliLongitude, $destinos)
        : null;
        if($responseJsonFromGoogleMatrixAPI){
            if($pessoa->isJson($responseJsonFromGoogleMatrixAPI)){
                $responseJsonFromGoogleMatrixAPI = json_decode($responseJsonFromGoogleMatrixAPI);
                // var_dump($responseJsonFromGoogleMatrixAPI);
                for($i=0; $i < sizeof($response); $i++){
                    if($responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->status != 'OK'){
                        $response[$i]->ERROAPIGOOGLEMAPS =$responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->status;
                    }else if($responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->status === 'OK'){
                        $response[$i]->DISTANCIAGOOGLETEXTUAL = $responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->distance->text;
                        $response[$i]->DISTANCIAGOOGLEMETROS = (double) $responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->distance->value;
                        $response[$i]->TEMPOESTIMADOGOOGLETEXTUAL = $responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->duration->text;
                        $response[$i]->TEMPOESTIMADOGOOGLESEGUNDOS = intval($responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->duration->value);
                    }
                    $response[$i]->DISTANCIAKMLINHARETA = !strlen($response[$i]->DISTANCIAGOOGLEMETROS)
                        ? $pessoa->calculaDistanciaAPartirDasLatitudesLongitudes($response[$i]->CLILATITUDE, $response[$i]->CLILONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE)
                        : null;
                }
            } else {
                for($i=0; $i < sizeof($response); $i++){
                    $response[$i]->ERROAPIGOOGLEMAPS = $responseJsonFromGoogleMatrixAPI;
                    if($response[$i]->LATITUDE && $response[$i]->LONGITUDE){
                        $response[$i]->DISTANCIAKMLINHARETA = $pessoa->calculaDistanciaAPartirDasLatitudesLongitudes($response[0]->CLILATITUDE, $response[0]->CLILONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE);
                    }
                }
            }
        }
        // return Handles::jsonResponse('true', 'Record list!', $responseJsonFromGoogleMatrixAPI);
        return Handles::jsonResponse('true', 'Record list!', $response);
    }
    /**
     * @OA\Get(
     *     path="/api/v1/produtos/promocoes/{pessoa}/cidade/{cidade}",
     *     tags={"Custom"},
     *     summary="Lista produtos em promoção por cidade",
     *     description="Retorna todos os produtos em promoções referente a cidade passado como parametro",
     *     @OA\Parameter(
     *         name="cidade",
     *         in="path",
     *         required=true,
     *         description="pesquisa utilizando parametro LIKE % %",
     *         @OA\Schema(
     *             type="string",
     *             example="%CURITIBA%"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="pessoa",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=12
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showProdutosPromocoesCidade($pessoa, $cidade){
        $sis_url = env('SISTEMA_URL');
        $sql =  "SELECT R.ID_RESTAURANTE, P.DESCRICAO, C.NOME, C.CIDADE, C.BAIRRO, R.OBS, R.PRECOVENDA, R.PRECOPROMOCAO, R.INICIOPROMOCAO, R.FIMPROMOCAO, R.ATIVO, ";
        $sql .= "IF( (I.ID <> '' and I.ID is not null), CONCAT('".$sis_url."','/images/doc/',LOWER(I.MODULO),'/',I.ID_DOC,'/',I.ID,'.jpg'), '') as LINK, ";
        $sql .= "'' AS DISTANCIAKM, CLI.ID as IDCLIENTE, CLI.LATITUDE AS CLILATITUDE, CLI.LONGITUDE AS CLILONGITUDE, C.LATITUDE, C.LONGITUDE, ";
        $sql .= "IF(FIND_IN_SET(WEEKDAY(CURDATE()), C.DIASEMANA) && ( ";
        $sql .= "(WEEKDAY(CURDATE()) < 5 && ((CURRENT_TIME() > C.INICIOMANHA && CURRENT_TIME() < C.FIMMANHA) || ";
        $sql .= "(CURRENT_TIME() > C.INICIOTARDE && CURRENT_TIME() < C.FIMTARDE))) || ";
        $sql .= "(WEEKDAY(CURDATE()) = 5 && ((CURRENT_TIME() > C.INICIOSABADOMANHA && CURRENT_TIME() < C.FIMSABADOMANHA) || ";
        $sql .= "(CURRENT_TIME() > C.INICIOSABADOTARDE && CURRENT_TIME() < C.FIMSABADOTARDE))) || ";
        $sql .= "(WEEKDAY(CURDATE()) = 6 && ((CURRENT_TIME() > C.INICIODOMINGOMANHA && CURRENT_TIME() < C.FIMDOMINGOMANHA) || ";
        $sql .= "(CURRENT_TIME() > C.INICIODOMINGOTARDE && CURRENT_TIME() < C.FIMDOMINGOTARDE)))),true,false) as ABERTO ";
        $sql .= "FROM EST_PRODUTO_RESTAURANTE R ";
        $sql .= "right join EST_PRODUTO P ON (P.ID=R.ID_PRODUTO) ";
        $sql .= "right join CRM_PESSOA C ON (C.ID=R.ID_RESTAURANTE) ";
        $sql .= "JOIN CRM_PESSOA CLI ON CLI.ID = $pessoa ";
        $sql .= "LEFT JOIN AMB_IMAGEM I ON R.ID=I.ID_DOC AND LOWER(I.MODULO) = 'est' ";
        $sql .= "WHERE R.ATIVO='S' AND (R.INICIOPROMOCAO <= CURDATE()) and (R.FIMPROMOCAO >= CURDATE()) AND PRECOPROMOCAO > 0 AND ";
        $sql .= "C.CIDADE LIKE '%". $cidade. "%' ";
        $sql .= "order by R.PRECOPROMOCAO";
        // var_dump($sql);
        $response = DB::select($sql);
        for($i=0; $i < sizeof($response); $i++){
            $response[$i]->DISTANCIAKM = $this->calculaDistanciaAPartirDasLatitudesLongitudes($response[$i]->CLILATITUDE, $response[$i]->CLILONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE);
            unset($response[$i]->PASSWORD);
            unset($response[$i]->SENHA);
            unset($response[$i]->USERLOGIN);
            unset($response[$i]->EMAIL);
            unset($response[$i]->EMAILSENHA);
        }
        return Handles::jsonResponse('true', 'Record list!', $response);
    }

    private function callGoogleMapsMatrixAPI($lat_inicial, $long_inicial, $destinos){
        $mapsKey = env('GOOGLE_MAPS_KEY');
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metrics&origins=$lat_inicial,$long_inicial&destinations=$destinos&key=$mapsKey&language=pt-BR";
        try{
            $ch = curl_init();
            if ($ch === false) {
                return 'Falha ao inicializar o curl';
            }
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $res = curl_exec($ch);

            if ($res === false) {
                return curl_error($ch);
            }
            curl_close($ch);
            return $res;
        }catch(Exception $e) {
            return  $e->getMessage();

        
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/produtos/promocoes/{pessoa}/{id}",
     *     tags={"Custom"},
     *     summary="Lista produtos em promoção de um respectivo restaurante",
     *     description="Retorna produtos em promoção referente ao id do restaurante passado como parametro",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example="1"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="pessoa",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=12
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showProdutosPromocoes($pessoa,$id)
    {
        $sql =  "SELECT R.ID_RESTAURANTE, P.DESCRICAO, C.NOME, C.CIDADE, C.BAIRRO, R.OBS, R.PRECOVENDA, R.PRECOPROMOCAO, R.INICIOPROMOCAO, R.FIMPROMOCAO, R.ATIVO, ";
        $sql .= "'' AS DISTANCIAKM, CLI.ID as IDCLIENTE, CLI.LATITUDE AS CLILATITUDE, CLI.LONGITUDE AS CLILONGITUDE, C.LATITUDE, C.LONGITUDE, ";
        $sql .= "IF(FIND_IN_SET(WEEKDAY(CURDATE()), C.DIASEMANA) && ( ";
        $sql .= "(WEEKDAY(CURDATE()) < 5 && ((CURRENT_TIME() > C.INICIOMANHA && CURRENT_TIME() < C.FIMMANHA) || ";
        $sql .= "(CURRENT_TIME() > C.INICIOTARDE && CURRENT_TIME() < C.FIMTARDE))) || ";
        $sql .= "(WEEKDAY(CURDATE()) = 5 && ((CURRENT_TIME() > C.INICIOSABADOMANHA && CURRENT_TIME() < C.FIMSABADOMANHA) || ";
        $sql .= "(CURRENT_TIME() > C.INICIOSABADOTARDE && CURRENT_TIME() < C.FIMSABADOTARDE))) || ";
        $sql .= "(WEEKDAY(CURDATE()) = 6 && ((CURRENT_TIME() > C.INICIODOMINGOMANHA && CURRENT_TIME() < C.FIMDOMINGOMANHA) || ";
        $sql .= "(CURRENT_TIME() > C.INICIODOMINGOTARDE && CURRENT_TIME() < C.FIMDOMINGOTARDE)))),true,false) as ABERTO ";
        $sql .= "FROM EST_PRODUTO_RESTAURANTE R ";
        $sql .= "inner join EST_PRODUTO P ON (P.ID=R.ID_PRODUTO) ";
        $sql .= "inner join CRM_PESSOA C ON (C.ID=R.ID_RESTAURANTE) ";
        $sql .= "JOIN CRM_PESSOA CLI ON CLI.ID = $pessoa ";
        $sql .= "WHERE R.ATIVO='S' AND (R.INICIOPROMOCAO <= CURDATE()) and (R.FIMPROMOCAO >= CURDATE()) AND PRECOPROMOCAO > 0 AND ";
        $sql .= "R.ID_RESTAURANTE = ".$id." ";
        $sql .= "order by R.PRECOPROMOCAO";

        $response = DB::select($sql);
        for($i=0; $i < sizeof($response); $i++){
            $response[$i]->DISTANCIAKM = $this->calculaDistanciaAPartirDasLatitudesLongitudes($response[$i]->CLILATITUDE, $response[$i]->CLILONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE);
            unset($response[$i]->PASSWORD);
            unset($response[$i]->SENHA);
            unset($response[$i]->USERLOGIN);
            unset($response[$i]->EMAIL);
            unset($response[$i]->EMAILSENHA);
        }
        return Handles::jsonResponse('true', 'Record list!', $response);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/produtos/maispedidos/{id}",
     *     tags={"Custom"},
     *     summary="Lista oS produtos mais pedidos de um respectivo restaurante",
     *     description="Retorna produtos mais pedidos referente ao id do restaurante passado como parametro",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example="1"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showProdutosMaisPedidos($id)
    {
        $sis_url = env('SISTEMA_URL');
        $sql =  "SELECT ID_PRODUTO_RESTAURANTE, P.DESCRICAO, R.TEMPO_PREPARO, R.PRECOVENDA, ";
        $sql .= "IF(((R.INICIOPROMOCAO <= CURDATE()) and (R.FIMPROMOCAO >= CURDATE())), R.PRECOPROMOCAO, 0) as PRECOPROMOCAO, R.OBS, ";
        $sql .= "IF( (I.ID <> '' and I.ID is not null), CONCAT('".$sis_url."','/images/doc/',LOWER(I.MODULO),'/',I.ID_DOC,'/',I.ID,'.jpg'), '') as LINK, ";
        $sql .= "SUM(QTATENDIDA) AS QUANTIDADE ";
        $sql .= "FROM FAT_PEDIDO_ITEM  PI ";
        $sql .= "INNER JOIN EST_PRODUTO_RESTAURANTE R ON R.ID=PI.ID_PRODUTO_RESTAURANTE ";
        $sql .= "INNER JOIN EST_PRODUTO P ON P.ID=R.ID_PRODUTO ";
        $sql .= "LEFT JOIN AMB_IMAGEM I ON R.ID=I.ID_DOC AND LOWER(I.MODULO) = 'est' ";
        $sql .= "WHERE R.ATIVO='S' AND R.ID_RESTAURANTE = ". $id." ";
        $sql .= "group by 1, 2, 3, 4, 5, 6, 7 ";
        $sql .= "order by QUANTIDADE";

        $response = DB::select($sql);

        return Handles::jsonResponse('true', 'Record list!', $response);

    }


    /**
     * @OA\Get(
     *     path="/api/v1/produtos/descricao/{id}/{descricao}",
     *     tags={"Custom"},
     *     summary="Retorna produtos pesquisando a descrição referente ao id do restaurante passado como parametro",
     *     description="Retorna produtos pesquisando a descrição referente ao id do restaurante passado como parametro",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example="1"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="descricao",
     *         in="path",
     *         required=true,
     *         description="pesquisa utilizando parametro LIKE % %",
     *         @OA\Schema(
     *             type="string",
     *             example="%SALADA%"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    public function showProdutosDescricao($id, $descricao)
    {
        $sis_url = env('SISTEMA_URL');
        $sql =  "SELECT P.DESCRICAO, R.ID as ID_PRODUTO_RESTAURANTE, C.NOME, C.CIDADE, C.BAIRRO, R.OBS, R.PRECOVENDA, R.INICIOPROMOCAO, R.FIMPROMOCAO, R.ATIVO, ";
        $sql .= "IF(((R.INICIOPROMOCAO <= CURDATE()) and (R.FIMPROMOCAO >= CURDATE())), R.PRECOPROMOCAO, 0) as PRECOPROMOCAO, ";
        $sql .= "IF( (I.ID <> '' and I.ID is not null), CONCAT('".$sis_url."','/images/doc/',LOWER(I.MODULO),'/',I.ID_DOC,'/',I.ID,'.jpg'), '') as LINK ";
        $sql .= "FROM EST_PRODUTO_RESTAURANTE R ";
        $sql .= "inner join EST_PRODUTO P ON (P.ID=R.ID_PRODUTO) ";
        $sql .= "inner join CRM_PESSOA C ON (C.ID=R.ID_RESTAURANTE) ";
        $sql .= "LEFT JOIN AMB_IMAGEM I ON R.ID=I.ID_DOC AND LOWER(I.MODULO) = 'est' ";
        $sql .= "WHERE R.ATIVO='S' AND ";
        $sql .= "R.ID_RESTAURANTE = ". $id." AND ";
        $sql .= "P.DESCRICAO LIKE '%". $descricao."%' ";
        $sql .= "order by R.PRECOPROMOCAO";

        $response = DB::select($sql);

        return Handles::jsonResponse('true', 'Record list!', $response);

    }
    /**
     * @OA\Get(
     *     path="/api/v1/produtos/cardapio/{id}/{tipo}",
     *     tags={"Custom"},
     *     summary="Lista produtos por grupo de um respectivo restaurante",
     *     description="Retorna produtos por tipo de grupo ( TIPOS: A-ADICIONAIS, P-PRATOS, B-BEBIDAS, S-SOBREMESAS) referente ao id do restaurante passado como parametro",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example="1"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="tipo",
     *         in="path",
     *         required=true,
     *         description="pesquisa utilizando parametro tipo grupo",
     *         @OA\Schema(
     *             type="string",
     *             example="P"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    public function showProdutosCardapio($id, $tipo, $idProduto = null){
        $sis_url = env('SISTEMA_URL');
        if($tipo == 'a' || $tipo == 'A'){
            $sql = "SELECT P.DESCRICAO, P.ID, R.ID AS ID_PRODUTO_RESTAURANTE, R.TEMPO_PREPARO, 
                        A.ID AS ID_PRODUTO_RESTAURANTE_ADICIONAL,
                        A.ID_PRODUTO_RESTAURANTE_ADICIONAL,
                        A.OBS AS OBS_DO_ADICIONAL,
                        PA.DESCRICAO AS DESCRICAO_DO_ADICIONAL,
                        A.PRECOVENDA AS PRECOVENDA_ADICIONAL,
                        PRA.PRECOVENDA AS PRECOVENDA_ADICIONAL_TABELA_PRODUTO_RESTAURANTE,
                        R.PRECOVENDA, R.OBS, R.INICIOPROMOCAO, R.FIMPROMOCAO, R.ATIVO,
                        C.NOME, C.CIDADE, C.BAIRRO,
                        IF(((R.INICIOPROMOCAO <= CURDATE()) and (R.FIMPROMOCAO >= CURDATE())), R.PRECOPROMOCAO, 0) as PRECOPROMOCAO, G.GRUPO, G.DESCRICAO as DESCGRUPO,
                        IF( (I.ID <> '' and I.ID is not null), CONCAT('$sis_url','/images/doc/',LOWER(I.MODULO),'/',I.ID_DOC,'/',I.ID,'.jpg'), '') as LINK
                    FROM EST_PRODUTO_RESTAURANTE R 
                    RIGHT JOIN EST_PRODUTO_RESTAURANTE_ADICIONAL A
                        ON A.ID_PRODUTO_RESTAURANTE = R.ID ";
                        is_null($idProduto) ? null : $sql .= " AND R.ID = $idProduto ";
                    $sql .= " LEFT JOIN EST_PRODUTO_RESTAURANTE PRA
                        ON PRA.ID = A.ID_PRODUTO_RESTAURANTE_ADICIONAL
                    LEFT JOIN EST_PRODUTO P 
                        ON P.ID = R.ID_PRODUTO 
                    LEFT JOIN EST_PRODUTO PA 
                        ON PA.ID = PRA.ID_PRODUTO 
                    LEFT JOIN EST_GRUPO G 
                        ON G.GRUPO = P.GRUPO
                        OR ((SELECT GG.TIPO FROM EST_GRUPO GG WHERE GG.GRUPO = G.GRUPOPAI) = P.GRUPO) 
                    LEFT JOIN CRM_PESSOA C 
                        ON C.ID = R.ID_RESTAURANTE
                    LEFT JOIN AMB_IMAGEM I 
                        ON R.ID = I.ID_DOC AND LOWER(I.MODULO) = 'est' 
                    WHERE R.ATIVO='S'
                        AND R.ID_RESTAURANTE = $id
                    ORDER BY P.DESCRICAO";
        }else{
            $sql =  "SELECT P.DESCRICAO, P.ID, R.ID AS ID_PRODUTO_RESTAURANTE, R.TEMPO_PREPARO, C.NOME, C.CIDADE, C.BAIRRO, R.OBS, R.PRECOVENDA, R.INICIOPROMOCAO, R.FIMPROMOCAO, R.ATIVO, ";
            $sql .= "IF(((R.INICIOPROMOCAO <= CURDATE()) and (R.FIMPROMOCAO >= CURDATE())), R.PRECOPROMOCAO, 0) as PRECOPROMOCAO, G.GRUPO, G.DESCRICAO as DESCGRUPO, ";
            $sql .= "IF( (I.ID <> '' and I.ID is not null), CONCAT('".$sis_url."','/images/doc/',LOWER(I.MODULO),'/',I.ID_DOC,'/',I.ID,'.jpg'), '') as LINK ";
            $sql .= "FROM EST_PRODUTO_RESTAURANTE R ";
            $sql .= "RIGHT JOIN EST_PRODUTO P ON (P.ID=R.ID_PRODUTO) ";
            $sql .= "LEFT JOIN EST_GRUPO G ON (G.GRUPO=P.GRUPO) or ((SELECT GG.TIPO FROM EST_GRUPO GG WHERE GG.GRUPO = G.GRUPOPAI) = P.GRUPO) ";
            $sql .= "RIGHT JOIN CRM_PESSOA C ON (C.ID=R.ID_RESTAURANTE) ";
            $sql .= "LEFT JOIN AMB_IMAGEM I ON R.ID=I.ID_DOC AND LOWER(I.MODULO) = 'est' ";
            $sql .= "WHERE R.ATIVO='S' AND ";
            $sql .= "R.ID_RESTAURANTE = ". $id." AND ";
            $sql .= "G.TIPO = '". $tipo."' ";
            $sql .= "order by P.DESCRICAO";
        }
        $response = DB::select($sql);
        return Handles::jsonResponse('true', 'Record list!', $response);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/produtos/pesquisa/{pessoa}/{cidade}/{descricao}",
     *     tags={"Custom"},
     *     summary="Lista restaurantes da {cidade} que possuem a descrição do produto similar à descrição passada como parâmetro",
     *     description="Retorna id dos restaurantes que possuem o produto",
     *     @OA\Parameter(
     *         name="cidade",
     *         in="path",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="curitiba"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="descricao",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="a"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="pessoa",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=12
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    public function pesquisaProdutos($pessoa, $cidade, $descricao){
        $cidade = (($cidade == '{cidade}') || (!strlen($cidade)))  ? '' : $cidade;
        $sql  = "SELECT PR.ID_RESTAURANTE, P.DESCRICAO, R.NOME, R.CIDADE, ";
        $sql .= "'' AS DISTANCIAKM, CLI.ID as IDCLIENTE, CLI.LATITUDE AS CLILATITUDE, CLI.LONGITUDE AS CLILONGITUDE, R.LATITUDE, R.LONGITUDE, ";
        $sql .= "IF(FIND_IN_SET(WEEKDAY(CURDATE()), R.DIASEMANA) && ( ";
        $sql .= "(WEEKDAY(CURDATE()) < 5 && ((CURRENT_TIME() > R.INICIOMANHA && CURRENT_TIME() < R.FIMMANHA) || ";
        $sql .= "(CURRENT_TIME() > R.INICIOTARDE && CURRENT_TIME() < R.FIMTARDE))) || ";
        $sql .= "(WEEKDAY(CURDATE()) = 5 && ((CURRENT_TIME() > R.INICIOSABADOMANHA && CURRENT_TIME() < R.FIMSABADOMANHA) || ";
        $sql .= "(CURRENT_TIME() > R.INICIOSABADOTARDE && CURRENT_TIME() < R.FIMSABADOTARDE))) || ";
        $sql .= "(WEEKDAY(CURDATE()) = 6 && ((CURRENT_TIME() > R.INICIODOMINGOMANHA && CURRENT_TIME() < R.FIMDOMINGOMANHA) || ";
        $sql .= "(CURRENT_TIME() > R.INICIODOMINGOTARDE && CURRENT_TIME() < R.FIMDOMINGOTARDE)))),true,false) as ABERTO ";
        $sql .= "FROM EST_PRODUTO_RESTAURANTE as PR ";
        $sql .= "RIGHT JOIN EST_PRODUTO as P on PR.ID_PRODUTO = P.ID ";
        $sql .= "RIGHT JOIN CRM_PESSOA as R on PR.ID_RESTAURANTE = R.ID ";
        $sql .= "JOIN CRM_PESSOA CLI ON CLI.ID = $pessoa ";
        $sql .= "WHERE R.PESSOA = 'R' ";
        $sql .= "AND P.DESCRICAO LIKE '%".$descricao."%' ";
        $sql .= "AND R.CIDADE LIKE '%".$cidade."%' ";
        $response = DB::select($sql);
        for($i=0; $i < sizeof($response); $i++){
            $response[$i]->DISTANCIAKM = $this->calculaDistanciaAPartirDasLatitudesLongitudes($response[$i]->CLILATITUDE, $response[$i]->CLILONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE);
            unset($response[$i]->PASSWORD);
            unset($response[$i]->SENHA);
            unset($response[$i]->USERLOGIN);
            unset($response[$i]->EMAIL);
            unset($response[$i]->EMAILSENHA);
        }
        return Handles::jsonResponse('true', 'Record list!', $response);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/produtos/imagens/{id}",
     *     tags={"Custom"},
     *     summary="Lista os caminhos das imagens do produto {id}",
     *     description="Retorna todas os caminhos das imagens do produto {id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID do Produto",
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    public function showImagensProduto($idProduto){
        $adm_url = 'http://'. getHostByName(getHostName()) . ':8000';
        $sql  = "SELECT P.ID as ID_PRODUTO, I.ID as ID_IMAGEM, I.ID_DOC as ID_PRODUTO_IMAGEM, P.DESCRICAO, E.ID_RESTAURANTE, E.OBS, I.DESTAQUE, IF( (I.ID_DOC <> '' and I.ID_DOC is not null), CONCAT('".$adm_url."','/images/',I.MODULO,'/',I.ID,'.jpg'), '') as LINK ";
        $sql .= "FROM EST_PRODUTO P ";
        $sql .= "LEFT JOIN EST_PRODUTO_RESTAURANTE E ON P.ID=E.ID_PRODUTO ";
        $sql .= "RIGHT JOIN AMB_IMAGEM I ON P.ID=I.ID_DOC "; //AND LOWER(I.MODULO) = 'crm' ";
        $sql .= "WHERE P.ID = " . $idProduto;
        try{
            $response = DB::select($sql);
            return $response;
        } catch (\Illuminate\Database\QueryException $exception) {
            return $exception;
        }
    }
    /**
     * @OA\Get(
     *     path="/api/v1/produtos/imagem/{id}",
     *     tags={"Custom"},
     *     summary="Lista o caminho da imagem destaque do produto {id}",
     *     description="Retorna o caminho da imagem destaque do produto {id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID do Produto",
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    public function showImagemDestaqueProduto($idProduto){
        $adm_url = 'http://'. getHostByName(getHostName()) . ':8000';
        $sql  = "SELECT P.ID as ID_PRODUTO, I.ID as ID_IMAGEM, I.ID_DOC as ID_PRODUTO_IMAGEM, P.DESCRICAO, I.DESTAQUE, IF( (I.ID_DOC <> '' and I.ID_DOC is not null), CONCAT('".$adm_url."','/images/',I.MODULO,'/',I.ID,'.jpg'), '') as LINK ";
        $sql .= "FROM EST_PRODUTO P ";
        $sql .= "RIGHT JOIN AMB_IMAGEM I ON P.ID=I.ID_DOC ";
        $sql .= "WHERE P.ID = " . $idProduto . " ";
        $sql .= "AND I.DESTAQUE = 'S' limit 1";
        try{
            $response = DB::select($sql);
            return $response;
        } catch (\Illuminate\Database\QueryException $exception) {
            return $exception;
        }
    }
    /**
     * @OA\Get(
     *     path="/api/v1/produtos/imagem/{idRestaurante}/{id}",
     *     tags={"Custom"},
     *     summary="Lista o caminho da imagem destaque do produto {id} do restaurante {idRestaurante}",
     *     description="Retorna o caminho da imagem destaque do produto {id} do restaurante {idRestaurante}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID do Produto",
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="idRestaurante",
     *         in="path",
     *         required=true,
     *         description="ID do Produto",
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    public function showImagemDestaqueProdutoRestaurante($idProduto,$idRestaurante){
        $adm_url = 'http://'. getHostByName(getHostName()) . ':8000';
        $sql  = "SELECT P.ID as ID_PRODUTO, I.ID as ID_IMAGEM, I.ID_DOC as ID_PRODUTO_IMAGEM, P.DESCRICAO, E.ID_RESTAURANTE, E.OBS, I.DESTAQUE, IF( (I.ID_DOC <> '' and I.ID_DOC is not null), CONCAT('".$adm_url."','/images/',I.MODULO,'/',I.ID,'.jpg'), '') as LINK ";
        $sql .= "FROM EST_PRODUTO P ";
        $sql .= "RIGHT JOIN AMB_IMAGEM I ON P.ID=I.ID_DOC ";
        $sql .= "LEFT JOIN EST_PRODUTO_RESTAURANTE E ON P.ID=E.ID_PRODUTO ";
        $sql .= "WHERE P.ID = " . $idProduto . " ";
        $sql .= "AND E.ID_RESTAURANTE = " . $idRestaurante . " ";
        $sql .= "AND I.DESTAQUE = 'S' limit 1 ";
        try{
            $response = DB::select($sql);
            return $response;
        } catch (\Illuminate\Database\QueryException $exception) {
            return $exception;
        }
    }

}
