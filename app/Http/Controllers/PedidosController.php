<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Utils\Handles;
use App\Models\pedidos;
use App\Models\pedidositens;
use App\Models\pedidositensadicionais;
use App\Models\pedidospesquisas;
use App\Models\pedidosstatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PedidosController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/pedidos/usuarios/{id}",
     *     tags={"Custom"},
     *     summary="Lista pedidos por usuário",
     *     description="Retorna todos os pedidos referente ao id do usuário passado como parametro",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example="1"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    /**
     * @OA\Get(
     *     path="/api/v1/pedidos/usuarios/{id}/{situacao}",
     *     tags={"Custom"},
     *     summary="Lista pedidos por usuário",
     *     description="Retorna todos os pedidos referente ao id do usuário passado como parametro",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example="1"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="situacao",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showPedidosUsuarios($id, $situacao = null){
        $response = $situacao 
            ? pedidos::select('FAT_PEDIDO.*','C.NOME AS CLIENTE','R.NOME AS RESTAURANTE')
                ->join('CRM_PESSOA as C', 'FAT_PEDIDO.ID_CLIENTE', '=', 'C.ID')
                ->join('CRM_PESSOA as R', 'FAT_PEDIDO.ID_RESTAURANTE', '=', 'R.ID')
                ->where('FAT_PEDIDO.SITUACAO', $situacao)
                ->where('FAT_PEDIDO.ID_CLIENTE', $id)
                ->orderBy('CREATED_AT','desc')
                ->get()
            : pedidos::select('FAT_PEDIDO.*','C.NOME AS CLIENTE','R.NOME AS RESTAURANTE')
                ->join('CRM_PESSOA as C', 'FAT_PEDIDO.ID_CLIENTE', '=', 'C.ID')
                ->join('CRM_PESSOA as R', 'FAT_PEDIDO.ID_RESTAURANTE', '=', 'R.ID')
                ->where('FAT_PEDIDO.ID_CLIENTE', '=', $id)
                ->orderBy('CREATED_AT','desc')
                ->get();
        return Handles::jsonResponse('true', 'Record list!', $response);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/pedidos/itens/{id}",
     *     tags={"Custom"},
     *     summary="Lista itens  da tabela pedidos",
     *     description="Retorna itens do pedido referente ao id do pedido passado como parametro",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showPedidosItens($id){
        $sis_url = env('SISTEMA_URL');
        $sql = "SELECT PIT.*, P.*, C.*, PR.OBS, PR.TEMPO_PREPARO, PIT.CREATED_AT AS CREATED_AT_ITEM, PIT.UPDATED_AT AS UPDATED_AT_ITEM, PIT.ID AS ITEM_ID, 
            IF( I.ID <> '', 
                CONCAT('$sis_url',
                    '/images/doc/',
                    LOWER(I.MODULO),
                    '/',
                    I.ID_DOC,
                    '/',
                    I.ID,
                    '.jpg'), '') AS LINK
        FROM FAT_PEDIDO P
        LEFT JOIN FAT_PEDIDO_ITEM PIT ON P.ID = PIT.PEDIDO_ID 
        LEFT JOIN EST_PRODUTO_RESTAURANTE PR ON PR.ID = PIT.ID_PRODUTO_RESTAURANTE 
        LEFT JOIN CRM_PESSOA C ON P.ID_CLIENTE = C.ID
        LEFT JOIN AMB_IMAGEM I ON PR.ID=I.ID_DOC AND LOWER(I.MODULO) = 'est' 
        WHERE PR.ATIVO = 'S' AND P.ID = $id";
        $response = DB::select($sql);
        return Handles::jsonResponse('true', 'Record list!', $response);
    }

    // public function showPedidosItens($id){
    //     $response = pedidos::select('FAT_PEDIDO.*','CRM_PESSOA.NOME', 'FAT_PEDIDO_ITEM.*')
    //     ->join('FAT_PEDIDO_ITEM', 'FAT_PEDIDO_ITEM.PEDIDO_ID', '=', 'FAT_PEDIDO.ID')
    //     ->join('CRM_PESSOA', 'FAT_PEDIDO.ID_CLIENTE', '=', 'CRM_PESSOA.ID')
    //     ->where('FAT_PEDIDO.ID', '=', $id)
    //     ->get();
    //     return Handles::jsonResponse('true', 'Record list!', $response);
    // }
    /**
     * @OA\Get(
     *     path="/api/v1/pedidos/{id}",
     *     tags={"Custom"},
     *     summary="Lista o pedido, clientes, produtos e avaliação do pedido {id}",
     *     description="Retorna os clientes, produtos e avalações do pedido {id} passado como parametro",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showPedidosItensEAdicionaisEAvaliacao($idPedido)    {
        $response = pedidos::select('FAT_PEDIDO.*','CRM_PESSOA.NOME','FAT_PEDIDO_ITEM.NRITEM','FAT_PEDIDO_ITEM.ID_PRODUTO_RESTAURANTE',
                'FAT_PEDIDO_ITEM.QTSOLICITADA','FAT_PEDIDO_ITEM.QTATENDIDA','FAT_PEDIDO_ITEM.VALORUNITARIO','FAT_PEDIDO_ITEM.VALORDESCONTO',
                'FAT_PEDIDO_ITEM.VALORTOTAL','FAT_PEDIDO_ITEM_ADICIONAL.VALOR','FAT_PEDIDO_ITEM_ADICIONAL.QUANTIDADE',
                'FAT_PEDIDO_PESQUISA.ID_PEDIDO','FAT_PEDIDO_PESQUISA.CREATED_AT',
                'FAT_PEDIDO_PESQUISA.UPDATED_AT','S1.PERGUNTA AS PERGUNTA1','FAT_PEDIDO_PESQUISA.RESPOSTA1',
                'S2.PERGUNTA AS PERGUNTA2','FAT_PEDIDO_PESQUISA.RESPOSTA2','S3.PERGUNTA AS PERGUNTA3',
                'FAT_PEDIDO_PESQUISA.RESPOSTA3','FAT_PEDIDO_PESQUISA.MEDIA')
            ->leftjoin('FAT_PEDIDO_ITEM', 'FAT_PEDIDO_ITEM.PEDIDO_ID','FAT_PEDIDO.ID')
            ->leftjoin('FAT_PEDIDO_ITEM_ADICIONAL', 'FAT_PEDIDO_ITEM.ID','FAT_PEDIDO_ITEM_ADICIONAL.ID_PEDIDO_ITEM')
            ->leftjoin('CRM_PESSOA', 'FAT_PEDIDO.ID_CLIENTE','CRM_PESSOA.ID')
            ->leftjoin('FAT_PEDIDO_PESQUISA', 'FAT_PEDIDO_PESQUISA.ID_PEDIDO','FAT_PEDIDO.ID')
            ->leftjoin('FAT_PERGUNTAS_STATUS as S1','FAT_PEDIDO_PESQUISA.PERGUNTA1','S1.ID')
            ->leftjoin('FAT_PERGUNTAS_STATUS as S2','FAT_PEDIDO_PESQUISA.PERGUNTA2','S2.ID')
            ->leftjoin('FAT_PERGUNTAS_STATUS as S3','FAT_PEDIDO_PESQUISA.PERGUNTA3','S3.ID')
            ->where('FAT_PEDIDO.ID',$idPedido)
            ->get();
        return Handles::jsonResponse('true', 'Record list!', $response);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/pedidos/itens",
     *     tags={"Custom"},
     *     summary="Adiciona um produto no pedido",
     *     description="Verifica se existe um pedido em aberto do cliente no restaurate que fez chekin, se não exister adiciona um pedido e inclui o novo item => STATUS PEDIDO: A - Aberto, B - Baixado => STATUS ITEM: 0 - Aguardando Confirmação, 1 - Aguardando Preparação, 2 - Cozinha, 3 - Preparado, 8 - Cancelado, 9 - Entregue",
     *     @OA\Response(
     *         response=201,
     *         description="Record Insert"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Record not insert - Missing Data"
     *     ),
     *     @OA\Parameter(
     *         name="idCliente",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_USUARIO_SOLICITANTE",
     *         in="query",
     *         description="Id do usuário que está fazendo o pedido, pode ser um garçom ou adm. Geralmente será o próprio cliente",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="idProdutoRestaurante",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="obs pedidoITEM"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="obs",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="obs PEDIDO"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="descProduto",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="descricao do item Pedido"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="quantidade",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="quantidadeAtendida",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="valorUnitario",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             example=100
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="valorTotal",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             example=100
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="idRestaurante",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="idMesa",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="idPedido",
     *         in="query",
     *         description="Se não for passado, pegará o ID do último pedido em aberto do cliente {idCliente}",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="idPedidoItem",
     *         in="query",
     *         description="Deve ser passado se o item do pedido está sofrendo edição",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */    
    
    public function storePedidosItens(Request $request){
        // status Item: Aberto, Baixado
        // status Item: Aberto, Aguardando Preparação, Preparado, Preparado, Entregue
        $modelPedido = "App\\Models\\pedidos";
        $modelPedidoItem = "App\\Models\\pedidositens";
        $modelProdutoRestaurante = "App\\Models\\restaurantesprodutos";
        $payload = $request->all();
        // array_key_exists('descricao', $payload) ? $payload['descricao'] : '';
        $idCliente = $payload['idCliente'];
        $idRestaurante = $payload['idRestaurante'];
        $idProdutoRestaurante = $payload['idProdutoRestaurante'];
        $idMesa = $payload['idMesa'];
        $quantidade = $payload['quantidade'];
        $idPedido = isset($payload['idPedido']) ? $payload['idPedido'] : false;
        $idPedidoItem = isset($payload['idPedidoItem']) ? $payload['idPedidoItem'] : false;
        $idPedidoItemPai = isset($payload['ID_PEDIDO_ITEM_PAI']) ? $payload['ID_PEDIDO_ITEM_PAI'] : false;
        $usuarioSolicitante = isset($payload['ID_USUARIO_SOLICITANTE']) ? $payload['ID_USUARIO_SOLICITANTE'] : $idCliente;
        //$adicionais = $payload['adicionais'];
        DB::beginTransaction();
        try {
            $produtoRestauranteResponse = $modelProdutoRestaurante::where('ID', $idProdutoRestaurante)
                    ->where('ID_RESTAURANTE', $idRestaurante)
                    ->where('ATIVO', 'S')
                    ->first();
            if(!$produtoRestauranteResponse){
                $produtoRestauranteResponse = $modelProdutoRestaurante::where('ID', $idProdutoRestaurante)
                    ->where('ID_RESTAURANTE', $idRestaurante)
                    ->where('ATIVO','<>','S')
                    ->first();
                if($produtoRestauranteResponse){
                    return Handles::jsonResponse('false', 'Registro não inserido, produto inativo!', $payload, 422);
                }else{ 
                    $produtoRestauranteResponse = $modelProdutoRestaurante::where('ID', $idProdutoRestaurante)->first();
                    return !$produtoRestauranteResponse 
                        ? Handles::jsonResponse('false', 'Registro não inserido, idProdutoRestaurante não encontrado!', $payload, 422)
                        : Handles::jsonResponse('false', 'Registro não inserido, idProdutoRestaurante não pertence ao restaurante informado!', $payload, 422);
                }
            }
            // var_dump($produtoRestauranteResponse['FIMPROMOCAO']);
            // var_dump($produtoRestauranteResponse['INICIOPROMOCAO']);
            // var_dump($produtoRestauranteResponse['INICIOPROMOCAO'] < $produtoRestauranteResponse['FIMPROMOCAO']);
            // var_dump(strtotime($produtoRestauranteResponse['INICIOPROMOCAO']->format('Y-m-d')));
            if($idPedidoItemPai){ // o produto é um adicional, então o pai é passado junto
                $produtoPai = $modelPedidoItem::select('ID_PRODUTO_RESTAURANTE')
                    ->where('ID', $idPedidoItemPai)
                    ->first();
                if(!$produtoPai){
                    DB::rollback();
                    return Handles::jsonResponse('false', 'Registro não inserido - Erro ao buscar produto vinculado!', $payload, 406);
                }
                if(!$produtoPai->ID_PRODUTO_RESTAURANTE){
                    DB::rollback();
                    return Handles::jsonResponse('false', 'Registro não inserido - Erro ao buscar produto vinculado!', $payload, 406);
                }
                $modelProdutoRestauranteAdicional = "App\\Models\\produtosrestaurantesadicionais";
                $produtoPai = $modelProdutoRestauranteAdicional::select('PRECOVENDA')
                    ->where('ID_PRODUTO_RESTAURANTE', $produtoPai->ID_PRODUTO_RESTAURANTE)
                    ->where('ID_PRODUTO_RESTAURANTE_ADICIONAL', $idProdutoRestaurante)
                    ->first();
                if(!$produtoPai){
                    DB::rollback();
                    return Handles::jsonResponse('false', 'Registro não inserido - Este produto não está cadastrado nos adicionais!', $payload, 406);
                }
                if(!$produtoPai->PRECOVENDA){
                    DB::rollback();
                    return Handles::jsonResponse('false', 'Registro não inserido - Erro ao buscar produto vinculado!', $payload, 406);
                }
                $valorTotalProdutos = $produtoPai->PRECOVENDA * $quantidade;
            } else {
                $valorProdutoRestaurante = $produtoRestauranteResponse['PRECOPROMOCAO']
                    ? $produtoRestauranteResponse['INICIOPROMOCAO']
                        ? $produtoRestauranteResponse['FIMPROMOCAO']
                            ?  (strtotime($produtoRestauranteResponse['INICIOPROMOCAO']->format('Y-m-d')) <= strtotime(date('Y-m-d')))
                            && (strtotime($produtoRestauranteResponse['FIMPROMOCAO']->format('Y-m-d')) >= strtotime(date('Y-m-d')))
                                ? $produtoRestauranteResponse['PRECOPROMOCAO']
                                : $produtoRestauranteResponse['PRECOVENDA']
                            : $produtoRestauranteResponse['PRECOVENDA']
                        : $produtoRestauranteResponse['PRECOVENDA']
                    : $produtoRestauranteResponse['PRECOVENDA'];
                $valorTotalProdutos = $valorProdutoRestaurante * $quantidade;
            }
            // pesquisa ultimo pedido em aberto do cliente no restaurante.
            $pedidoResponse = $idPedido ? $modelPedido::where('ID', $idPedido)
                    ->where('SITUACAO', '=', 'A')
                    ->first()
                : $modelPedido::where('ID_CLIENTE', $idCliente)
                    ->where('ID_RESTAURANTE', '=', $idRestaurante)
                    ->where('SITUACAO', '=', 'A')
                    // ->whereNull('DATA_FECHAMENTO')
                    ->first();
            if(!$pedidoResponse) {
                // inclui pedido se não existir
                $pedido['ID_CLIENTE'] = $idCliente;
                $pedido['ID_RESTAURANTE'] = $idRestaurante;
                $pedido['ID_MESA'] = $idMesa;
                $pedido['SITUACAO'] = 'A';
                $pedido['OBS'] = isset($payload['obs']) ? $payload['obs'] : null;
                $pedido['EMISSAO'] = date('Y-m-d H:i:s');
                $pedido['CREATED_AT'] = date('Y-m-d H:i:s');
                $pedido['TOTAL'] = $valorTotalProdutos;
                $pedido['USERINSERT'] = $usuarioSolicitante;
                $pedidoItem['NRITEM'] = 1;
                $pedidoResponse = $modelPedido::create($pedido);
            } else {
                $pedidoResponse['UPDATED_AT'] = date('Y-m-d H:i:s');
                $pedidoResponse['USERUPDATE'] = $usuarioSolicitante;
                // var_dump($pedidoResponse['TOTAL']); //ambas funcionam
                // var_dump($pedidoResponse->TOTAL); //ambas funcionam
                is_null($pedidoResponse->TOTAL) ? $pedidoResponse->TOTAL = 0 : null;
                $pedidoResponse['TOTAL'] += $valorTotalProdutos;
                isset($payload['obs']) ? $pedidoResponse['OBS'] = $payload['obs'] : null;

                //se é edição de pedidoitem, salva o pedido depois
                $idPedidoItem ? null : $pedidoResponse = $pedidoResponse->save();
            }
            if($idPedidoItem){ //alteração
                $pedidoItem = $modelPedidoItem::where('ID', $idPedidoItem)
                    ->first();
                $pedidoResponse['TOTAL'] -= $pedidoItem['VALORTOTAL']; //subtrai do pedido o valor do pedidoitem que sofreu alteração
                $pedidoResponse = $pedidoResponse->save();
                $pedidoItem['USERUPDATE'] = $usuarioSolicitante;
                $pedidoItem['UPDATED_AT'] = date('Y-m-d H:i:s');
                isset($payload['descProduto']) ? $pedidoItem['DESCRICAO'] = $payload['descProduto'] : null;
                $pedidoItem['QTSOLICITADA'] = $quantidade;
                isset($payload['OBSERVACAO']) ? $pedidoItem['OBSERVACAO'] = $payload['OBSERVACAO'] : null;
                $pedidoItem['QTATENDIDA'] = $quantidade;
                $pedidoItem['VALORUNITARIO'] = $idPedidoItemPai ? $produtoPai->PRECOVENDA : $valorProdutoRestaurante;
                $pedidoItem['VALORTOTAL'] = $valorTotalProdutos;
                $idPedidoItemPai ? $pedidoItem['ID_PEDIDO_ITEM_PAI'] = $idPedidoItemPai : null;
                $pedidoItem['STATUS'] = 0;
                $pedidoItemResponse = $pedidoItem->save();
                $pedidoItemResponse ?
                    $pedidoItemResponse = $modelPedidoItem::where('ID', $idPedidoItem)
                        ->first()
                            : null;
            } else {
                // inclui pedido item
                $pedidoResponse = $idPedido
                    ? $modelPedido::where('ID', $idPedido)
                        ->where('SITUACAO', '=', 'A')
                        ->first()
                    : $modelPedido::where('ID_CLIENTE', '=', $idCliente)
                        ->where('ID_RESTAURANTE', '=', $idRestaurante)
                        ->where('SITUACAO', '=', 'A')// ->whereNull('DATA_FECHAMENTO')
                        ->first();  
                isset($pedidoItem['NRITEM'])
                    ? null 
                    : $nrItem = $modelPedidoItem::orderBy('NRITEM','desc')
                        ->where('PEDIDO_ID', $pedidoResponse->ID)
                        ->first();
                isset($pedidoItem['NRITEM']) ? null : $pedidoItem['NRITEM'] = !is_null($nrItem) ? ($nrItem->NRITEM + 1) : 1;
                $pedidoItem['PEDIDO_ID'] = $pedidoResponse->ID;
                $pedidoItem['ID_PRODUTO_RESTAURANTE'] = $payload['idProdutoRestaurante'];
                isset($payload['descProduto']) ? $pedidoItem['DESCRICAO'] =  $payload['descProduto'] : null;
                isset($payload['OBSERVACAO']) ? $pedidoItem['OBSERVACAO'] = $payload['OBSERVACAO'] : null;
                $idPedidoItemPai ? $pedidoItem['ID_PEDIDO_ITEM_PAI'] = $idPedidoItemPai : null;
                $pedidoItem['QTSOLICITADA'] = $quantidade;
                $pedidoItem['VALORUNITARIO'] = $idPedidoItemPai ? $produtoPai->PRECOVENDA : $valorProdutoRestaurante;
                $pedidoItem['VALORTOTAL'] = $valorTotalProdutos;
                $pedidoItem['STATUS'] = 0;
                $pedidoItem['CREATED_AT'] = date('Y-m-d H:i:s');
                $pedidoItem['USERINSERT'] = $usuarioSolicitante;
                $pedidoItemResponse = $modelPedidoItem::create($pedidoItem);
            }
            // Save response data
            $response = array(
                'pedidoResponse' => $pedidoResponse,
                'pedidoItemResponse' => $pedidoItemResponse,
            );
            DB::commit();
            return Handles::jsonResponse('true', 'Registro inserido!', $response, 201);

        } catch (\Illuminate\Database\QueryException $exception) {
            DB::rollback();
            // You can check get the details of the error using `errorInfo`:
            return Handles::jsonResponse('false', 'Registro não inserido, dados faltantes!', $exception, 422);
        }
    }
    /**
     * @OA\Post(
     *     path="/api/v1/pedidos/itens/adicionais",
     *     tags={"Custom"},
     *     summary="Adiciona um adicional ao item do pedido",
     *     description="Verifica se existe um pedido em aberto do cliente no restaurate que fez chekin, se não exister adiciona um pedido e inclui o novo item => STATUS PEDIDO: A - Aberto, B - Baixado => STATUS ITEM: 0 - Aguardando Confirmação, 1 - Aguardando Preparação, 2 - Cozinha, 3 - Preparado, 8 - Cancelado, 9 - Entregue",
     *     @OA\Response(
     *         response=201,
     *         description="Record Insert"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Record not insert - Missing Data"
     *     ),
     *     @OA\Parameter(
     *         name="ID_CLIENTE",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PEDIDO_ITEM",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=19
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PRODUTO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="VALOR",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="QUANTIDADE",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_USUARIO_SOLICITANTE",
     *         in="query",
     *         description="ID_USUARIO_SOLICITANTE, pode ser o cliente ou id do garçom ou do admin",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    public function storePedidosItensAdicionais(Request $request){
        $payload = $request->all();
        $response = '';
        $idCliente = $payload['ID_CLIENTE'];
        $ID_PEDIDO_ITEM = $payload['ID_PEDIDO_ITEM'];
        $ID_PRODUTO = $payload['ID_PRODUTO'];
        $VALOR = isset($payload['VALOR']) ? $payload['VALOR'] : null;
        //quantidade teoricamente vai ser sempre 1
        $QUANTIDADE = isset($payload['QUANTIDADE']) ? $payload['QUANTIDADE'] : 1;
        $usuarioSolicitante = isset($payload['ID_USUARIO_SOLICITANTE']) ? $payload['ID_USUARIO_SOLICITANTE'] : $idCliente;
        DB::beginTransaction();
        try {
            $pedidoItemResponse = pedidositens::where('ID', $ID_PEDIDO_ITEM)
                ->first();
            $pedidoResponse = pedidos::where('ID', $pedidoItemResponse->PEDIDO_ID)
                ->where('SITUACAO', 'A')
                ->first();
                if($pedidoItemResponse && $pedidoResponse) {
                $valorPedidoAtualizado['VALOR'] = $pedidoResponse[0]->TOTAL + ($QUANTIDADE * $VALOR);
                $atualizaValorPedidoResponse = pedidos::where('ID', $pedidoResponse[0]->ID)
                    ->update($valorPedidoAtualizado);
                if(!$atualizaValorPedidoResponse){
                    DB::rollback();
                    return Handles::jsonResponse('false', 'Falha ao atualizar o valor do pedido.', $pedidoResponse, 422);
                }
                $pedidoItemAdicional['ID_PEDIDO_ITEM'] = $ID_PEDIDO_ITEM;
                $pedidoItemAdicional['ID_PRODUTO'] = $ID_PRODUTO;
                $pedidoItemAdicional['QUANTIDADE'] = $QUANTIDADE;
                $pedidoItemAdicional['VALOR'] = $VALOR;
                $pedidoItemAdicional['USERINSERT'] = $usuarioSolicitante;
                $pedidoItemAdicional['CREATED_AT'] = date('Y-m-d H:i:s');
                $pedidoItemAdicionalResponse = pedidositensadicionais::create($pedidoItemAdicional);
                DB::commit();
                return Handles::jsonResponse('true', 'Record insert!', $response, 201);
            }
            DB::rollback();
            $mensagemErro = !$pedidoItemResponse ? 'Item não encontrado' : !$pedidoResponse ? 'Pedido não encontrado' : 'Erro desconhecido';
            return Handles::jsonResponse('false', 'Pedido ou item do pedido não encontrados.', $mensagemErro, 422);
        }catch (\Illuminate\Database\QueryException $exception) {
            DB::rollback();
            return Handles::jsonResponse('false', 'Record not insert - Missing Data!', $exception, 422);
        }
    }


    /**
     * @OA\Get(
     *     path="/api/v1/pedidos/itens/status/{id}/{status}",
     *     tags={"Custom"},
     *     summary="Lista todos os itens de um pedido por status",
     *     description="Retorna uma lista com todos itens de um determinado pedido com o STATUS informado ",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showPedidosItensStatus($id, $status){
        $model = "App\\Models\\pedidositens";
        $response = $model::orderBy('ID')
            ->where('PEDIDO_ID', '=', $id)
            ->where('STATUS', '=', $status)
            ->get();

        return Handles::jsonResponse('true', 'Record list!', $response);
    }
    /**
     * @OA\Get(
     *     path="/api/v1/pedidos/satisfacao/{id}",
     *     tags={"Custom"},
     *     summary="Lista a avaliação de acordo com o pedido {id}",
     *     description="Retorna uma lista com as perguntas apresentadas ao cliente, e suas respostas, de acordo com o id do pedido ",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    public function showAvaliacaoPedido($idPedido){
        try {
            $response = pedidospesquisas::select('FAT_PEDIDO_PESQUISA.ID_PEDIDO','FAT_PEDIDO_PESQUISA.CREATED_AT',
                    'FAT_PEDIDO_PESQUISA.UPDATED_AT','S1.PERGUNTA AS PERGUNTA1','FAT_PEDIDO_PESQUISA.RESPOSTA1',
                    'S2.PERGUNTA AS PERGUNTA2','FAT_PEDIDO_PESQUISA.RESPOSTA2','S3.PERGUNTA AS PERGUNTA3',
                    'FAT_PEDIDO_PESQUISA.RESPOSTA3','FAT_PEDIDO_PESQUISA.MEDIA','FAT_PEDIDO_PESQUISA.OBSERVACAO')
                ->leftjoin('FAT_PERGUNTAS_STATUS as S1','FAT_PEDIDO_PESQUISA.PERGUNTA1','S1.ID')
                ->leftjoin('FAT_PERGUNTAS_STATUS as S2','FAT_PEDIDO_PESQUISA.PERGUNTA2','S2.ID')
                ->leftjoin('FAT_PERGUNTAS_STATUS as S3','FAT_PEDIDO_PESQUISA.PERGUNTA3','S3.ID')
                ->where('FAT_PEDIDO_PESQUISA.ID_PEDIDO',$idPedido)
                ->first();
            return Handles::jsonResponse('true', 'Record list!', $response);
        }catch (\Illuminate\Database\QueryException $exception) {
            return Handles::jsonResponse('false', 'error!', $exception);
        }
    }
    /**
     * @OA\Get(
     *     path="/api/v1/pedidos/satisfacao/cliente/{id}",
     *     tags={"Custom"},
     *     summary="Lista as avaliações de acordo com o cliente {id}",
     *     description="Retorna uma lista com as perguntas apresentadas ao cliente, e suas respostas, de acordo com o id do cliente ",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=16
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    public function showAvaliacoesPedidosCliente($idCliente){
        try {
            $response = pedidospesquisas::select('FAT_PEDIDO_PESQUISA.ID_PEDIDO','FAT_PEDIDO_PESQUISA.CREATED_AT',
                    'FAT_PEDIDO_PESQUISA.UPDATED_AT','S1.PERGUNTA AS PERGUNTA1','FAT_PEDIDO_PESQUISA.RESPOSTA1',
                    'S2.PERGUNTA AS PERGUNTA2','FAT_PEDIDO_PESQUISA.RESPOSTA2','S3.PERGUNTA AS PERGUNTA3',
                    'FAT_PEDIDO_PESQUISA.RESPOSTA3','FAT_PEDIDO_PESQUISA.MEDIA','FAT_PEDIDO_PESQUISA.OBSERVACAO')
                ->leftjoin('FAT_PERGUNTAS_STATUS as S1','FAT_PEDIDO_PESQUISA.PERGUNTA1','S1.ID')
                ->leftjoin('FAT_PERGUNTAS_STATUS as S2','FAT_PEDIDO_PESQUISA.PERGUNTA2','S2.ID')
                ->leftjoin('FAT_PERGUNTAS_STATUS as S3','FAT_PEDIDO_PESQUISA.PERGUNTA3','S3.ID')
                ->leftjoin('FAT_PEDIDO as P','FAT_PEDIDO_PESQUISA.ID_PEDIDO','P.ID')
                ->where('P.ID_CLIENTE',$idCliente)
                ->get();
            return Handles::jsonResponse('true', 'Record list!', $response);
        }catch (\Illuminate\Database\QueryException $exception) {
            return Handles::jsonResponse('false', 'error!', $exception);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/v1/pedidos/satisfacao",
     *     tags={"Custom"},
     *     summary="Salva a avaliação do pedido {id}",
     *     description="vincula o pedido ID com as perguntas e respostas da avaliação do pedido",
     *     @OA\Parameter(
     *         name="ID_PEDIDO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PERGUNTA1",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PERGUNTA2",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PERGUNTA3",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=3
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="RESPOSTA1",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=5
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="RESPOSTA2",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=3
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="RESPOSTA3",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="teste de observação via swagger"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    public function storeAvaliacaoPedido(Request $request)
    {
        $payload = $request->all();
        $response = '';
        try {
            $idPedido = $payload['ID_PEDIDO'];
            $pedidoPesquisa['ID_PEDIDO'] = $idPedido;
            $pedidoPesquisa['PERGUNTA1'] = $payload['PERGUNTA1'];
            $pedidoPesquisa['PERGUNTA2'] = $payload['PERGUNTA2'];
            $pedidoPesquisa['PERGUNTA3'] = $payload['PERGUNTA3'];
            $totalRespostas = $payload['RESPOSTA1'] ? 1 : 0;
            $totalRespostas += $payload['RESPOSTA2'] ? 1 : 0;
            $totalRespostas += $payload['RESPOSTA3'] ? 1 : 0;
            $totalRespostas ? $totalRespostas : 1; //para não dividir por 0
            $pedidoPesquisa['RESPOSTA1'] = $payload['RESPOSTA1'];
            $pedidoPesquisa['RESPOSTA2'] = $payload['RESPOSTA2'];
            $pedidoPesquisa['RESPOSTA3'] = $payload['RESPOSTA3'];
            $pedidoPesquisa['RESPOSTA3'] = $payload['RESPOSTA3'];
            $pedidoPesquisa['OBSERVACAO'] = $payload['OBSERVACAO'];
            $soma = $payload['RESPOSTA1'] ? $payload['RESPOSTA1'] : 0;
            $soma += $payload['RESPOSTA2'] ? $payload['RESPOSTA2'] : 0;
            $soma += $payload['RESPOSTA2'] ? $payload['RESPOSTA3'] : 0;
            $pedidoPesquisa['MEDIA'] = $totalRespostas ? round( ($soma / $totalRespostas), 2) : null;
            $pedidoResponse = pedidospesquisas::where('ID_PEDIDO', $idPedido)->first();
            if(!$pedidoResponse) {
                $pedidoPesquisa['CREATED_AT'] = date('Y-m-d H:i:s');
                $response = pedidospesquisas::create($pedidoPesquisa);
            }else{
                $pedidoPesquisa['UPDATED_AT'] = date('Y-m-d H:i:s');
                $pedidoResponse->fill($pedidoPesquisa);
                $response = $pedidoResponse->save();
            }
            return Handles::jsonResponse('true', ($pedidoResponse ? 'Avaliação atualizada!' : 'Avaliação salva!'), $response);
        }catch (\Illuminate\Database\QueryException $exception) {
            return Handles::jsonResponse('false', 'error!', $exception);
        }
    }
}
