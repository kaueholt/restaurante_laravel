<?php

namespace App\Http\Controllers;

use App\Mail\ContactUs;
use App\Mail\OrderRequested;
use App\Mail\RegisterCompleted;
use App\Mail\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailRulesController extends Controller
{
    /**
     * @description Send an email when user is registered
     * @param $userMail
     */
    public function sendMailRegister($userMail, $userId)
    {
        $isMailSended = Mail::to($userMail)
            ->locale('pt-br')
            ->bcc(['marcio.sergio@admservice.com.br'])
            ->send(new RegisterCompleted($userId));

        return $isMailSended;
    }

    /**
     * @param $userMail
     * @return mixed
     */
    public function sendMailResetPassword($userMail, $userId)
    {
        return Mail::to($userMail)
            ->bcc(['marcio.sergio@admservice.com.br'])
            ->locale('pt-br')
            ->send(new ResetPassword($userId));
    }

    /**
     * @param $userMail
     * @return mixed
     */
    public function sendMailContactUs(Request $request)
    {
        $data = $request->all();

        $isMailSended = Mail::to('marcio.sergio@admservice.com.br')
            ->bcc(['marcio.sergio@admservice.com.br'])
            ->locale('pt-br')
            ->send(new ContactUs($data['name'], $data['message'], $data['cnpj'], $data['emailContact'], $data['telefoneContact'], $data['nameBusiness'], $data['cityBusiness'], $data['telephoneBusiness']));
        return $isMailSended;
    }

}