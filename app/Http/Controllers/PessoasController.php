<?php

namespace App\Http\Controllers;

use App\Models\pessoas;
use Illuminate\Http\Request;
use App\Http\Controllers\Utils\Handles;
use Illuminate\Support\Facades\DB;

class PessoasController extends Controller{
    /**
     * @OA\Get(
     *     path="/api/v1/geoloc/{latitude}/{longitude}/{cidade}",
     *     tags={"Custom"},
     *     summary="Lista de registros do tipo restaurante da tabela pessoas por latitude e longitude",
     *     description="Retorna restaurantes aplicando filtro de latitude e longitude",
     *     @OA\Parameter(
     *         name="cidade",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="CURITIBA"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="latitude",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             example=-25.4322481
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="longitude",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             example=-49.2660084
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    /**
     * @OA\Get(
     *     path="/api/v1/pessoas/{pessoa}/cidade/{cidade}",
     *     tags={"Custom"},
     *     summary="Lista de registros do tipo restaurante da tabela pessoas por cidade",
     *     description="Retorna restaurantes aplicando filtro LIKE em cidade",
     *     @OA\Parameter(
     *         name="cidade",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="CURITIBA"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="pessoa",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=12
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    /**
     * @OA\Get(
     *     path="/api/v1/pessoas/{pessoa}/cidade/{cidade}/{nomeRestaurante}",
     *     tags={"Custom"},
     *     summary="Lista de registros do tipo restaurante da tabela pessoas por cidade",
     *     description="Retorna restaurantes aplicando filtro LIKE em cidade",
     *     @OA\Parameter(
     *         name="cidade",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="CURITIBA"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="pessoa",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=12
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="nomeRestaurante",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="mize"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    public function calculaRating($pessoa){
        $sql = "SELECT SUM(MEDIA)/COUNT(1) as RATE FROM FAT_PEDIDO_PESQUISA PP
                WHERE PP.ID_PEDIDO IN (
                    SELECT P.ID FROM FAT_PEDIDO P WHERE P.ID_RESTAURANTE = $pessoa)";
        $response = DB::select($sql);
        return $response; 
                
    }    

    public function showByCidade($pessoa,$cidade,$nomeRestaurante = null){
        // $cidade = "%" . $cidade . "%";
        //https://admservice.com.br/mizefood/images/doc/crm/8/40.jpg
        $sis_url = env('SISTEMA_URL');
        // $sql  = "SELECT P.CODIGO, I.IDANUNCIO, I.DESTAQUE, IF( (I.ID <> '' and I.ID is not null), CONCAT('".$adm_url."','/images/doc/',I.MODULO,'/',I.ID,'.jpg'), '') as LINK ";
        $sql  = "SELECT P.*,
                    '' AS DISTANCIAGOOGLEMETROS,
                    '' AS DISTANCIAGOOGLETEXTUAL,
                    '' AS TEMPOESTIMADOGOOGLETEXTUAL,
                    '' AS TEMPOESTIMADOGOOGLESEGUNDOS,
                    '' AS DISTANCIAKMLINHARETA,
                    NULL AS ERROAPIGOOGLEMAPS,
                    C.ID as IDCLIENTE,
                    CAST(C.LATITUDE AS DECIMAL(10,7)) AS CLILATITUDE,
                    CAST(C.LONGITUDE AS DECIMAL(10,7)) AS CLILONGITUDE, 
                    IF( (I.ID <> '' and I.ID is not null),
                            CONCAT('".$sis_url."',
                                '/images/doc/',
                                LOWER(I.MODULO),
                                '/',
                                I.ID_DOC,
                                '/',
                                I.ID,
                                '.jpg'),
                            '')
                        AS LINK,
                    IF(FIND_IN_SET(WEEKDAY(CURDATE()), P.DIASEMANA) && (
                        (WEEKDAY(CURDATE()) < 5 && ((CURRENT_TIME() > P.INICIOMANHA && CURRENT_TIME() < P.FIMMANHA) ||
                        (CURRENT_TIME() > P.INICIOTARDE && CURRENT_TIME() < P.FIMTARDE))) ||
                        (WEEKDAY(CURDATE()) = 5 && ((CURRENT_TIME() > P.INICIOSABADOMANHA && CURRENT_TIME() < P.FIMSABADOMANHA) ||
                        (CURRENT_TIME() > P.INICIOSABADOTARDE && CURRENT_TIME() < P.FIMSABADOTARDE))) ||
                        (WEEKDAY(CURDATE()) = 6 && ((CURRENT_TIME() > P.INICIODOMINGOMANHA && CURRENT_TIME() < P.FIMDOMINGOMANHA) ||
                        (CURRENT_TIME() > P.INICIODOMINGOTARDE && CURRENT_TIME() < P.FIMDOMINGOTARDE)))),true,false) as ABERTO
                    FROM CRM_PESSOA P
                    JOIN CRM_PESSOA C ON C.ID = $pessoa
                    LEFT JOIN AMB_IMAGEM I ON P.ID=I.ID_DOC AND UPPER(I.MODULO) = 'CRM'
                    WHERE P.PESSOA = 'R'
                    AND P.CIDADE LIKE '%$cidade%'";
        is_null($nomeRestaurante) ? null : $sql .= "AND (P.NOME LIKE '%".$nomeRestaurante."%' OR P.NOMEREDUZIDO LIKE '%".$nomeRestaurante."%')";

        // print_r($sql);
        $response = DB::select($sql);
        if(!$response){
            return Handles::jsonResponse('false', 'Nenhum restaurante encontrado!',[]);
        }
        $destinos = '';
        $cliLatitude = strlen($response[0]->CLILATITUDE) ? (double) $response[0]->CLILATITUDE : null;
        $cliLongitude = strlen($response[0]->CLILONGITUDE) ? (double) $response[0]->CLILONGITUDE : null;
        for($i=0; $i < sizeof($response); $i++){            
            if($response[$i]->ABERTO){
                $diaDaSemanaHoje = date('w');
                $horaMinutoAgora = date('H:i');
                $horaMinutoAgoraEmSegundosUnix = strtotime(date('H:i'));
                switch($diaDaSemanaHoje){
                    case 0: //domingo
                        if($horaMinutoAgora < $response[$i]->FIMDOMINGOMANHA)
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMDOMINGOMANHA))) - $horaMinutoAgoraEmSegundosUnix;
                        else
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMDOMINGOTARDE))) - $horaMinutoAgoraEmSegundosUnix;
                    break;
                    case 6: //sabado
                        if($horaMinutoAgora < $response[$i]->FIMDOMINGOMANHA)
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMSABADOMANHA))) - $horaMinutoAgoraEmSegundosUnix;
                        else
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMSABADOTARDE))) - $horaMinutoAgoraEmSegundosUnix;
                    break;
                    default:
                        if($horaMinutoAgora < $response[$i]->FIMMANHA)
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMMANHA))) - $horaMinutoAgoraEmSegundosUnix;
                        else
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMTARDE))) - $horaMinutoAgoraEmSegundosUnix;
                        
                    break;
                }
                $minutes = floor(($diffUnix % 3600) / 60);
                $hours = floor($diffUnix / 3600);
                $diff = date('H:i',strtotime($hours.':'.$minutes));
                $response[$i]->HORAMINUTOPARAFECHAR = $diff;
            }
            // $response[$i]->DISTANCIAKMLINHARETA = $this->calculaDistanciaAPartirDasLatitudesLongitudes($response[$i]->CLILATITUDE, $response[$i]->CLILONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE);
            $response[$i]->LATITUDE = strlen($response[$i]->LATITUDE) ? (double) $response[$i]->LATITUDE : null;
            $response[$i]->LONGITUDE = strlen($response[$i]->LONGITUDE) ? (double) $response[$i]->LONGITUDE : null;
            $response[$i]->CLILATITUDE = strlen($cliLatitude) ? (double) $cliLatitude : null;
            $response[$i]->CLILONGITUDE = strlen($cliLongitude) ? (double) $cliLongitude : null;
            $response[$i]->AVALIACAO = $this->calculaRating($response[$i]->ID);
            $response[$i]->AVALIACAO = isset($response[$i]->AVALIACAO[0]) ? isset($response[$i]->AVALIACAO[0]->RATE) ? $response[$i]->AVALIACAO[0]->RATE : null : null;
            $destinos .= $response[$i]->LATITUDE .','. $response[$i]->LONGITUDE .'|';
            unset($response[$i]->PASSWORD);
            unset($response[$i]->SENHA);
            unset($response[$i]->USERLOGIN);
            unset($response[$i]->EMAIL);
            unset($response[$i]->EMAILSENHA);
        }
        $destinos = strlen($destinos) && strlen($cliLatitude) && strlen($cliLongitude)
        ? substr($destinos, 0, -1)
        : null;
        $responseJsonFromGoogleMatrixAPI = strlen($destinos)
        ? $this->callGoogleMapsMatrixAPI($cliLatitude, $cliLongitude, $destinos)
        : null;
        if($responseJsonFromGoogleMatrixAPI){
            if($this->isJson($responseJsonFromGoogleMatrixAPI)){
                $responseJsonFromGoogleMatrixAPI = json_decode($responseJsonFromGoogleMatrixAPI);
                // var_dump($responseJsonFromGoogleMatrixAPI);
                for($i=0; $i < sizeof($response); $i++){
                    if($responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->status != 'OK'){
                        $response[$i]->ERROAPIGOOGLEMAPS =$responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->status;
                    }else if($responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->status === 'OK'){
                        $response[$i]->DISTANCIAGOOGLETEXTUAL = $responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->distance->text;
                        $response[$i]->DISTANCIAGOOGLEMETROS = (double) $responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->distance->value;
                        $response[$i]->TEMPOESTIMADOGOOGLETEXTUAL = $responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->duration->text;
                        $response[$i]->TEMPOESTIMADOGOOGLESEGUNDOS = intval($responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->duration->value);
                    }
                    $response[$i]->DISTANCIAKMLINHARETA = !strlen($response[$i]->DISTANCIAGOOGLEMETROS)
                        ? $this->calculaDistanciaAPartirDasLatitudesLongitudes($response[$i]->CLILATITUDE, $response[$i]->CLILONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE)
                        : null;
                }
            } else {
                for($i=0; $i < sizeof($response); $i++){
                    $response[$i]->ERROAPIGOOGLEMAPS = $responseJsonFromGoogleMatrixAPI;
                    if($response[$i]->LATITUDE && $response[$i]->LONGITUDE){
                        $response[$i]->DISTANCIAKMLINHARETA = $this->calculaDistanciaAPartirDasLatitudesLongitudes($response[0]->CLILATITUDE, $response[0]->CLILONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE);
                    }
                }
            }
        }
        // return Handles::jsonResponse('true', 'Record list!', $responseJsonFromGoogleMatrixAPI);
        return Handles::jsonResponse('true', 'Record list!', $response);
    }

    public function showByCoordinates($latitude,$longitude,$cidade,$sortByRating = false,$nomeRestaurante = null){
        // $cidade = "%" . $cidade . "%";
        //https://admservice.com.br/mizefood/images/doc/crm/8/40.jpg
        $sis_url = env('SISTEMA_URL');
        // $sql  = "SELECT P.CODIGO, I.IDANUNCIO, I.DESTAQUE, IF( (I.ID <> '' and I.ID is not null), CONCAT('".$adm_url."','/images/doc/',I.MODULO,'/',I.ID,'.jpg'), '') as LINK ";
        $sql  = "SELECT P.*,
                    '' AS DISTANCIAGOOGLEMETROS,
                    '' AS DISTANCIAGOOGLETEXTUAL,
                    '' AS TEMPOESTIMADOGOOGLETEXTUAL,
                    '' AS TEMPOESTIMADOGOOGLESEGUNDOS,
                    '' AS DISTANCIAKMLINHARETA,
                    NULL AS ERROAPIGOOGLEMAPS,
                    CAST($latitude AS DECIMAL(10,7)) AS CLILATITUDE,
                    CAST($longitude AS DECIMAL(10,7)) AS CLILONGITUDE, 
                    IF( (I.ID <> '' and I.ID is not null),
                            CONCAT('".$sis_url."',
                                '/images/doc/',
                                LOWER(I.MODULO),
                                '/',
                                I.ID_DOC,
                                '/',
                                I.ID,
                                '.jpg'),
                            '')
                        AS LINK,
                    IF(FIND_IN_SET(WEEKDAY(CURDATE()), P.DIASEMANA) && (
                        (WEEKDAY(CURDATE()) < 5 && ((CURRENT_TIME() > P.INICIOMANHA && CURRENT_TIME() < P.FIMMANHA) ||
                        (CURRENT_TIME() > P.INICIOTARDE && CURRENT_TIME() < P.FIMTARDE))) ||
                        (WEEKDAY(CURDATE()) = 5 && ((CURRENT_TIME() > P.INICIOSABADOMANHA && CURRENT_TIME() < P.FIMSABADOMANHA) ||
                        (CURRENT_TIME() > P.INICIOSABADOTARDE && CURRENT_TIME() < P.FIMSABADOTARDE))) ||
                        (WEEKDAY(CURDATE()) = 6 && ((CURRENT_TIME() > P.INICIODOMINGOMANHA && CURRENT_TIME() < P.FIMDOMINGOMANHA) ||
                        (CURRENT_TIME() > P.INICIODOMINGOTARDE && CURRENT_TIME() < P.FIMDOMINGOTARDE)))),true,false) as ABERTO
                    FROM CRM_PESSOA P
                    LEFT JOIN AMB_IMAGEM I ON P.ID=I.ID_DOC AND UPPER(I.MODULO) = 'CRM'
                    WHERE P.PESSOA = 'R'
                    AND P.CIDADE LIKE '%$cidade%'";
        is_null($nomeRestaurante) ? null : $sql .= "AND (P.NOME LIKE '%".$nomeRestaurante."%' OR P.NOMEREDUZIDO LIKE '%".$nomeRestaurante."%')";

        // print_r($sql);
        $response = DB::select($sql);
        if(!$response){
            return Handles::jsonResponse('false', 'Nenhum restaurante encontrado!',[]);
        }
        $destinos = '';
        $cliLatitude = strlen($response[0]->CLILATITUDE) ? (double) $response[0]->CLILATITUDE : null;
        $cliLongitude = strlen($response[0]->CLILONGITUDE) ? (double) $response[0]->CLILONGITUDE : null;
        for($i=0; $i < sizeof($response); $i++){            
            if($response[$i]->ABERTO){
                $diaDaSemanaHoje = date('w');
                $horaMinutoAgora = date('H:i');
                $horaMinutoAgoraEmSegundosUnix = strtotime(date('H:i'));
                switch($diaDaSemanaHoje){
                    case 0: //domingo
                        if($horaMinutoAgora < $response[$i]->FIMDOMINGOMANHA)
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMDOMINGOMANHA))) - $horaMinutoAgoraEmSegundosUnix;
                        else
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMDOMINGOTARDE))) - $horaMinutoAgoraEmSegundosUnix;
                    break;
                    case 6: //sabado
                        if($horaMinutoAgora < $response[$i]->FIMDOMINGOMANHA)
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMSABADOMANHA))) - $horaMinutoAgoraEmSegundosUnix;
                        else
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMSABADOTARDE))) - $horaMinutoAgoraEmSegundosUnix;
                    break;
                    default:
                        if($horaMinutoAgora < $response[$i]->FIMMANHA)
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMMANHA))) - $horaMinutoAgoraEmSegundosUnix;
                        else
                            $diffUnix = strtotime(date('H:i:s',strtotime($response[$i]->FIMTARDE))) - $horaMinutoAgoraEmSegundosUnix;
                        
                    break;
                }
                $minutes = floor(($diffUnix % 3600) / 60);
                $hours = floor($diffUnix / 3600);
                $diff = date('H:i',strtotime($hours.':'.$minutes));
                $response[$i]->HORAMINUTOPARAFECHAR = $diff;
            }
            // $response[$i]->DISTANCIAKMLINHARETA = $this->calculaDistanciaAPartirDasLatitudesLongitudes($response[$i]->CLILATITUDE, $response[$i]->CLILONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE);
            $response[$i]->LATITUDE = strlen($response[$i]->LATITUDE) ? (double) $response[$i]->LATITUDE : null;
            $response[$i]->LONGITUDE = strlen($response[$i]->LONGITUDE) ? (double) $response[$i]->LONGITUDE : null;
            $response[$i]->CLILATITUDE = strlen($cliLatitude) ? (double) $cliLatitude : null;
            $response[$i]->CLILONGITUDE = strlen($cliLongitude) ? (double) $cliLongitude : null;
            $response[$i]->AVALIACAO = $this->calculaRating($response[$i]->ID);
            $response[$i]->AVALIACAO = isset($response[$i]->AVALIACAO[0]) ? isset($response[$i]->AVALIACAO[0]->RATE) ? $response[$i]->AVALIACAO[0]->RATE : null : null;
            $destinos .= $response[$i]->LATITUDE .','. $response[$i]->LONGITUDE .'|';
            unset($response[$i]->PASSWORD);
            unset($response[$i]->SENHA);
            unset($response[$i]->USERLOGIN);
            unset($response[$i]->EMAIL);
            unset($response[$i]->EMAILSENHA);
        }
        $destinos = strlen($destinos) && strlen($cliLatitude) && strlen($cliLongitude)
        ? substr($destinos, 0, -1)
        : null;
        $responseJsonFromGoogleMatrixAPI = strlen($destinos)
        ? $this->callGoogleMapsMatrixAPI($cliLatitude, $cliLongitude, $destinos)
        : null;
        if($responseJsonFromGoogleMatrixAPI){
            if($this->isJson($responseJsonFromGoogleMatrixAPI)){
                $responseJsonFromGoogleMatrixAPI = json_decode($responseJsonFromGoogleMatrixAPI);
                // var_dump($responseJsonFromGoogleMatrixAPI);
                for($i=0; $i < sizeof($response); $i++){
                    if($responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->status != 'OK'){
                        $response[$i]->ERROAPIGOOGLEMAPS =$responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->status;
                    }else if($responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->status === 'OK'){
                        $response[$i]->DISTANCIAGOOGLETEXTUAL = $responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->distance->text;
                        $response[$i]->DISTANCIAGOOGLEMETROS = (double) $responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->distance->value;
                        $response[$i]->TEMPOESTIMADOGOOGLETEXTUAL = $responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->duration->text;
                        $response[$i]->TEMPOESTIMADOGOOGLESEGUNDOS = intval($responseJsonFromGoogleMatrixAPI->rows[0]->elements[$i]->duration->value);
                    }
                    $response[$i]->DISTANCIAKMLINHARETA = !strlen($response[$i]->DISTANCIAGOOGLEMETROS)
                        ? $this->calculaDistanciaAPartirDasLatitudesLongitudes($response[$i]->CLILATITUDE, $response[$i]->CLILONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE)
                        : null;
                }
            } else {
                for($i=0; $i < sizeof($response); $i++){
                    $response[$i]->ERROAPIGOOGLEMAPS = $responseJsonFromGoogleMatrixAPI;
                    if($response[$i]->LATITUDE && $response[$i]->LONGITUDE){
                        $response[$i]->DISTANCIAKMLINHARETA = $this->calculaDistanciaAPartirDasLatitudesLongitudes($response[0]->CLILATITUDE, $response[0]->CLILONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE);
                    }
                }
            }
        }
        if($sortByRating){
            usort($response, function($a, $b){
                return $a->AVALIACAO < $b->AVALIACAO;
            });
        } else {
            usort($response, function($a, $b)
            {
                return $a->DISTANCIAGOOGLEMETROS > $b->DISTANCIAGOOGLEMETROS;
                // return $a->DISTANCIAGOOGLEMETROS > $b->DISTANCIAGOOGLEMETROS
                //     ?   $b->DISTANCIAGOOGLEMETROS
                //         ?   ($a->DISTANCIAGOOGLEMETROS > $b->DISTANCIAGOOGLEMETROS)
                //         :   $b->DISTANCIAKMLINHARETA
                //             ?   ($a->DISTANCIAGOOGLEMETROS > $b->DISTANCIAKMLINHARETA)
                //             :   ($a->ID > $b->ID)
                //     :   $a->DISTANCIAKMLINHARETA
                //         ?   $b->DISTANCIAGOOGLEMETROS
                //             ?   ($a->DISTANCIAKMLINHARETA > $b->DISTANCIAGOOGLEMETROS)
                //             :   $b->DISTANCIAKMLINHARETA
                //                 ?   ($a->DISTANCIAKMLINHARETA > $b->DISTANCIAKMLINHARETA)
                //                 :   ($a->ID > $b->ID)
                //         :   ($a->ID > $b->ID);
            });
        }
        // return Handles::jsonResponse('true', 'Record list!', $responseJsonFromGoogleMatrixAPI);
        return Handles::jsonResponse('true', 'Record list!', $response);
    }

    public function calculaDistanciaAPartirDasLatitudesLongitudes($lat_inicial, $long_inicial, $lat_final, $long_final){
        $d2r = 0.017453292519943295769236;

        $dlong = ($long_final - $long_inicial) * $d2r;
        $dlat = ($lat_final - $lat_inicial) * $d2r;

        $temp_sin = sin($dlat/2.0);
        $temp_cos = cos($lat_inicial * $d2r);
        $temp_sin2 = sin($dlong/2.0);

        $a = ($temp_sin * $temp_sin) + ($temp_cos * $temp_cos) * ($temp_sin2 * $temp_sin2);
        $c = 2.0 * atan2(sqrt($a), sqrt(1.0 - $a));

        return round(6368.1 * $c,2);
    }

    // private function callGoogleMapsMatrixAPI($lat_inicial, $long_inicial, $destinos){
    //     $mapsKey = env('GOOGLE_MAPS_KEY');
    //     var_dump(file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=metrics&origins=$lat_inicial,$long_inicial&destinations=$destinos&key=$mapsKey"));
    //     // return file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=metrics&origins=$lat_inicial,$long_inicial&destinations=$destinos&key=$mapsKey");
    // }
 
    private function callGoogleMapsMatrixAPI($lat_inicial, $long_inicial, $destinos){
        $mapsKey = env('GOOGLE_MAPS_KEY');
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metrics&origins=$lat_inicial,$long_inicial&destinations=$destinos&key=$mapsKey&language=pt-BR";
        // $ch = curl_init("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=-25.4322481,-49.2660084&destinations=-25.4322481,-49.2660084&key=");
        try{
            $ch = curl_init();
            if ($ch === false) {
                return 'Falha ao inicializar o curl';
            }
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $res = curl_exec($ch);

            if ($res === false) {
                return curl_error($ch);
                // return curl_errno($ch)
            }
            curl_close($ch);
            return $res;
        }catch(Exception $e) {

            // trigger_error(sprintf(
            //     'Curl failed with error #%d: %s',
            //     $e->getCode(), $e->getMessage()),
            //     E_USER_ERROR);
            return  $e->getMessage();

        
        }
    }
    
        
    // public function showByCidade($cidade){
    //     $response = pessoas::orderBy('NOME')
    //         ->where('CIDADE', 'LIKE', "%". $cidade ."%")
    //         ->where('PESSOA','R')
    //         ->get();
    //     return Handles::jsonResponse('true', 'Record list!', $response);
    // }

    /**
     * @OA\Get(
     *     path="/api/v1/pessoas/telefone/{telefone}",
     *     tags={"Custom"},
     *     summary="Lista de registro da tabela pessoas por telefone",
     *     description="Retorna pessoas por telefone",
     *     @OA\Parameter(
     *         name="telefone",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="41988044284"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showByTelefone($telefone){
        // $telefone = preg_match_all('!\d+!', $telefone, $matches);
        $telefone = preg_replace('/[^0-9]/', '', $telefone);
        $response = pessoas::select('CRM_PESSOA.EMAIL')
        // $response = pessoas::select('CRM_PESSOA.*','AMB_USUARIO.USUARIO AS USER.USUARIO','AMB_USUARIO.NOME AS USER.NOME',
        //         'AMB_USUARIO.LOGIN AS USER.LOGIN','AMB_USUARIO.NOMEREDUZIDO AS USER.NOMEREDUZIDO','AMB_USUARIO.ID_PESSOA AS USER.ID_PESSOA',
        //         'AMB_USUARIO.SENHA AS USER.SENHA','AMB_USUARIO.SITUACAO AS USER.SITUACAO','AMB_USUARIO.TIPO AS USER.TIPO',
        //         'AMB_USUARIO.GRUPO AS USER.GRUPO','AMB_USUARIO.SMTP AS USER.SMTP','AMB_USUARIO.EMAIL AS USER.EMAIL',
        //         'AMB_USUARIO.EMAILSENHA AS USER.EMAILSENHA')
            // ->rightjoin('AMB_USUARIO','AMB_USUARIO.ID_PESSOA','CRM_PESSOA.ID')
            ->where('CRM_PESSOA.FONE', $telefone)
            ->orWhere('CRM_PESSOA.CELULAR', $telefone)
            ->first();
        return Handles::jsonResponse('true', 'Record list!', $response);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/pessoas/cidade/bairro/{cidade}/{bairro}",
     *     tags={"Custom"},
     *     summary="Lista de registro da tabela pessoas por cidade e bairro",
     *     description="Retorna retaurantes aplicando filtro LIKE de cidade e bairro, ",
     *     @OA\Parameter(
     *         name="cidade",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="CURITIBA"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="bairro",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="CENTRO"
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showCidadeBairro($cidade, $bairro){
        $query = pessoas::orderBy('NOME');
        $query->where('CIDADE', 'LIKE', $cidade."%");
        if ($bairro != ''){
            $query->where('BAIRRO', 'LIKE', "%".$bairro."%");
        }
        $response =  $query->get();
        return Handles::jsonResponse('true', 'Record list!', $response);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/pessoas/cupons/{id}",
     *     tags={"Custom"},
     *     summary="Lista todos os cupuns que foram disponibilizados para o cliente",
     *     description="Retorna uma lista com todos os CUPONS que foram disponibilizados pelo cliente, ",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=4
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showCupons($id){
        $sql =  "SELECT P.ID AS CUPOMPESSOA_ID, C.ID AS CUPOM_ID, C.DESCRICAO, C.VALOR, C.VALIDADEINICIO, C.VALIDADEFIM, P.UTILIZADO, ";
        $sql .=  "IF(((C.VALIDADEINICIO <= CURDATE()) and (C.VALIDADEFIM >= CURDATE()) and (P.UTILIZADO = false)), 'Ativo', 'Inativo') as STATUS ";
        $sql .=  "FROM FAT_CUPOM_PESSOA P ";
        $sql .= "right join FAT_CUPOM C ON C.ID=P.ID_CUPOM ";
        $sql .= "WHERE P.ID_PESSOA = " . $id . " ";
        $response = DB::select($sql);
        return Handles::jsonResponse('true', 'Record list!', $response);
    }

    public function isJson($string) {
        json_decode($string);
        return strlen($string) > 2 && (json_last_error() == JSON_ERROR_NONE);
    }
}
