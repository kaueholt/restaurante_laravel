<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Utils\Handles;

class DefaultController extends Controller
{
    public function index(Request $request, $table)
    {
        $model = "App\\Models\\".$table;
        $response = $model::all();
        if($response){
            unset($response['PASSWORD']);
            unset($response['SENHA']);
            unset($response['EMAILSENHA']);
            unset($response['FONE']);
            unset($response['CELULAR']);
        }
        return Handles::jsonResponse('true', 'Register list!', $response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request, $table){
        $model = "App\\Models\\".$table;
        $payload = $request->all();
        $payload['CREATED_AT'] = date('Y-m-d H:i:s');
        DB::beginTransaction();
        try {
            $query = new $model();
            //verifica se nos parâmetros está sendo passada a chave primária, se estiver, verifica se já não existe chave igual
            $chavePrimariaJaExiste = isset($payload[$query->primaryKey]) ? $model::find($payload[$query->primaryKey]) : null;
            if ($chavePrimariaJaExiste){
                DB::rollback();
                return Handles::jsonResponse('false', 'Registro não inserido - Chave '. $query->primaryKey .'->\''.$payload[$query->primaryKey] .'\' já existe!', $chavePrimariaJaExiste, 421);
            }
            if($table == 'pedidositens'){
                $NRITEM = $model::select('NRITEM')->where('PEDIDO_ID',$payload['PEDIDO_ID'])->orderBy('NRITEM','desc')->first();
                $NRITEM = $NRITEM ? $NRITEM->NRITEM ? $NRITEM->NRITEM : 1 : 1;
                $payload['NRITEM'] = $NRITEM + 1;
                if(isset($payload['ID_PEDIDO_ITEM_PAI'])){ // o produto é um adicional, então o pai é passado junto
                    $produtoPai = $model::select('ID_PRODUTO_RESTAURANTE')
                    ->where('ID',$payload['ID_PEDIDO_ITEM_PAI'])
                    ->first();
                    if(!$produtoPai){
                        DB::rollback();
                        return Handles::jsonResponse('false', 'Registro não inserido - Erro ao buscar produto vinculado!', $payload, 406);
                    }
                    if(!$produtoPai->ID_PRODUTO_RESTAURANTE){
                        DB::rollback();
                        return Handles::jsonResponse('false', 'Registro não inserido - Erro ao buscar produto vinculado!', $payload, 406);
                    }
                    $modelProdutoRestauranteAdicional = "App\\Models\\produtosrestaurantesadicionais";
                    $produtoPai = $modelProdutoRestauranteAdicional::select('PRECOVENDA')
                        ->where($ID_PRODUTO_RESTAURANTE, $produtoPai->ID_PRODUTO_RESTAURANTE)
                        ->where($ID_PRODUTO_RESTAURANTE_ADICIONAL, $payload['ID_PRODUTO_RESTAURANTE'])
                        ->first();
                    if(!$produtoPai){
                        DB::rollback();
                        return Handles::jsonResponse('false', 'Registro não inserido - Este produto não !', $payload, 406);
                    }
                    if(!$produtoPai->PRECOVENDA){
                        DB::rollback();
                        return Handles::jsonResponse('false', 'Registro não inserido - Erro ao buscar produto vinculado!', $payload, 406);
                    }
                    if($produtoPai->PRECOVENDA - $payload['VALORUNITARIO'] > 1 || $produtoPai->PRECOVENDA - $payload['VALORUNITARIO'] < -1){
                        DB::rollback();
                        return Handles::jsonResponse('false', 'O valor do adicional não condiz com o valor tabelado!', array('payload' => $payload['VALORUNITARIO'], 'tabela' => $produtoPai->PRECOVENDA), 407);
                    }
                    $payload['QTSOLICITADA'] ? null : $payload['QTSOLICITADA'] = 1;
                    $payload['VALORDESCONTO'] ? null : $payload['VALORDESCONTO'] = 0;
                    $payload['VALORUNITARIO'] ? null : $payload['VALORTOTAL'] / $payload['QTSOLICITADA'];
                    if( (($payload['VALORTOTAL'] - (($payload['VALORUNITARIO'] * $payload['QTSOLICITADA']) - $payload['VALORDESCONTO']) > 1 )) || 
                        (($payload['VALORTOTAL'] - (($payload['VALORUNITARIO'] * $payload['QTSOLICITADA']) - $payload['VALORDESCONTO']) < -1 ))){
                        DB::rollback();
                        return Handles::jsonResponse('false', 'O valor enviado não bate com o total calculado!', array('VALORTOTAL' => $payload['VALORTOTAL'], 'calculado' => (($payload['VALORUNITARIO'] * $payload['QTSOLICITADA']) - $payload['VALORDESCONTO'])), 408);
                    }
                }
            }else if($table == 'pessoas'){
                // preg_match_all('!\d+!', $payload['CNPJCPF'], $CNPJCPF);
                $CNPJCPFnumeros = preg_replace('/[^0-9]/', '', $payload['CNPJCPF']);
                $queryCPF = "SELECT ID 
                                FROM CRM_PESSOA 
                                WHERE CAST(
                                    REPLACE(
                                        REPLACE(
                                            REPLACE(CNPJCPF, '-', '')
                                            , ',', '')
                                        , '.', '')
                                    as UNSIGNED INTEGER)
                                = CAST($CNPJCPFnumeros as UNSIGNED) LIMIT 1";
                $cpfExists = DB::select($queryCPF);
                if($cpfExists){
                    return Handles::jsonResponse(false, 'CPF já cadastrado', $payload['CNPJCPF'], 403);
                }
                $sqlAPI_GOOGLE_KEY = "SELECT API_GOOGLE_KEY FROM AMB_PARAMETRO WHERE API_GOOGLE_KEY IS NOT NULL LIMIT 1";
                $responseAPI_GOOGLE_KEY = DB::select($sqlAPI_GOOGLE_KEY);
                if(!isset($responseAPI_GOOGLE_KEY[0]->API_GOOGLE_KEY)){
                    DB::rollback();
                    return Handles::jsonResponse(false, 'Nenhuma chave de API do google maps configurada!', $responseAPI_GOOGLE_KEY, 400);
                }
                $APIKEY = $responseAPI_GOOGLE_KEY[0]->API_GOOGLE_KEY;
                $urlMapsGeocodingAPI = str_replace (" ","+","https://maps.googleapis.com/maps/api/geocode/json?address='{$payload['ENDERECO']},{$payload['NUMERO']},{$payload['CIDADE']},{$payload['UF']}&key={$APIKEY}");
                $googleAPI = curl_init();
                curl_setopt($googleAPI, CURLOPT_URL, $urlMapsGeocodingAPI);
                curl_setopt($googleAPI, CURLOPT_RETURNTRANSFER, true);
                $googleAPI = json_decode(curl_exec($googleAPI));

                if(!isset($googleAPI->results[0])){
                    DB::rollback();
                    return Handles::jsonResponse(false, isset($googleAPI->status) ? $googleAPI->status : 'Endereço inválido!', $googleAPI, 400);
                }

                $payload['LATITUDE'] = round($googleAPI->results[0]->geometry->location->lat, 7);
                $payload['LONGITUDE'] = round($googleAPI->results[0]->geometry->location->lng, 7);
            }
            $query->fill($payload);
            if(!$query->save()){
                DB::rollback();
                return Handles::jsonResponse('true', 'Falha ao inserir!', $query, 401);
            } 
            DB::commit();
            return Handles::jsonResponse(true, 'Registro inserido! Tabela '.$query->table.', '.$query->primaryKey.' '. $query->getKey(), $query, 200);
        } catch (\Illuminate\Database\QueryException $exception) {
            DB::rollback();
            // You can check get the details of the error using `errorInfo`:
            return Handles::jsonResponse('false', 'Registro não inserido - Dados faltantes!', $exception, 422);
        } catch (\InvalidArgumentException $exception){
            DB::rollback();
            return Handles::jsonResponse('false', 'Registro não inserido - Dados incorretos!', $exception, 422);
        }
    }

    public function show($table, $id)
    {
        $model = "App\\Models\\".$table;
        $response = $model::find($id);

        if(!$response) {
            unset($response['PASSWORD']);
            unset($response['SENHA']);
            unset($response['EMAILSENHA']);
            unset($response['FONE']);
            unset($response['CELULAR']);
            return Handles::jsonResponse('false', 'Record not found!', $response, 404);
        }
        return Handles::jsonResponse('true', 'Record found!', $response);
    }

    public function update(Request $request, $table, $id)
    {
        $model = "App\\Models\\".$table;
        $payload = $request->all();
        $payload['UPDATED_AT'] = date('Y-m-d H:i:s');
        DB::beginTransaction();
        try {
            $response = $model::find($id);

            if(!$response) {
                DB::rollback();
                return Handles::jsonResponse('false', 'Record not found!', $response, 404);
            }
            if($table == 'pessoas' && (array_key_exists('ENDERECO',$payload) || array_key_exists('NUMERO',$payload) || array_key_exists('CIDADE',$payload) || array_key_exists('UF',$payload))){
                $sqlAPI_GOOGLE_KEY = "SELECT API_GOOGLE_KEY FROM AMB_PARAMETRO WHERE API_GOOGLE_KEY IS NOT NULL LIMIT 1";
                $responseAPI_GOOGLE_KEY = DB::select($sqlAPI_GOOGLE_KEY);
                if(!isset($responseAPI_GOOGLE_KEY[0]->API_GOOGLE_KEY)){
                    DB::rollback();
                    return Handles::jsonResponse(false, 'Nenhuma chave de API do google maps configurada!', $responseAPI_GOOGLE_KEY, 400);
                }
                $APIKEY = $responseAPI_GOOGLE_KEY[0]->API_GOOGLE_KEY;
                $urlMapsGeocodingAPI = str_replace (" ","+","https://maps.googleapis.com/maps/api/geocode/json?address='{$payload['ENDERECO']},{$payload['NUMERO']},{$payload['CIDADE']},{$payload['UF']}&key={$APIKEY}");
                $googleAPI = curl_init();
                curl_setopt($googleAPI, CURLOPT_URL, $urlMapsGeocodingAPI);
                curl_setopt($googleAPI, CURLOPT_RETURNTRANSFER, true);
                $googleAPI = json_decode(curl_exec($googleAPI));

                if(!isset($googleAPI->results[0])){
                    DB::rollback();
                    return Handles::jsonResponse(false, isset($googleAPI->status) ? $googleAPI->status : 'Endereço inválido!', $googleAPI, 400);
                }
                $payload['LATITUDE'] = round($googleAPI->results[0]->geometry->location->lat, 7);
                $payload['LONGITUDE'] = round($googleAPI->results[0]->geometry->location->lng, 7);
            }

            $response->fill($payload);
            if(!$response->save()){
                DB::rollback();
                return Handles::jsonResponse('true', 'Falha ao inserir!', $response, 401);
            } 
            DB::commit();
            return Handles::jsonResponse(true, 'Registro '.$response->getKey().' atualizado!', $response, 200);
        } catch (\Illuminate\Database\QueryException $exception) {
            DB::rollback();
            // You can check get the details of the error using `errorInfo`:
            return Handles::jsonResponse('false', 'Record not update - Validation exception!', $exception, 405);
        }
    }

    public function destroy($table, $id){
        $model = "App\\Models\\".$table;
        DB::beginTransaction();
        try {
            $response = $model::find($id);
            if(!$response) {
                DB::rollback();
                return Handles::jsonResponse('false', 'Record not found!', $response, 404);
            } 
            if($table == 'pedidositens'){                
                if(is_null($response->STATUS)){
                    DB::rollback();
                    return Handles::jsonResponse('false', 'Favor solicitar o cancelamento deste produto com um funcionário.', $response, 420);
                }
                if($response->STATUS > 1){
                    DB::rollback();
                    return Handles::jsonResponse('false', 'Favor solicitar o cancelamento deste produto com um funcionário.', $response, 425);
                }
                $totalPedidoItem = $response->VALORTOTAL;
                $modelPedido = "App\\Models\\pedidos";
                $pedido = $modelPedido::where('ID',$response->PEDIDO_ID)->first();
                $adicionaisDesteItem = $model::where('ID_PEDIDO_ITEM_PAI', $response->ID)->get();
                $totalAdicionais = 0;
                if($adicionaisDesteItem){
                    for($i=0; $i < sizeof($adicionaisDesteItem); $i++){
                        if(is_null($adicionaisDesteItem[$i]->STATUS)){
                            DB::rollback();
                            return Handles::jsonResponse('false', 'Favor solicitar o cancelamento deste produto com um funcionário. Erro ao excluir adicionais deste produto.', $response, 430);
                        }
                        if($adicionaisDesteItem[$i]->STATUS > 1){
                            DB::rollback();
                            return Handles::jsonResponse('false', 'Favor solicitar o cancelamento deste produto com um funcionário. Erro ao excluir adicionais deste produto.', $response, 435);
                        }
                        $totalAdicionais += $adicionaisDesteItem[$i]->VALORTOTAL ? $adicionaisDesteItem[$i]->VALORTOTAL : 0;
                        $deleteAdicionalResponse = $adicionaisDesteItem[$i]->delete();
                        if(!$deleteAdicionalResponse){
                            DB::rollback();
                            return Handles::jsonResponse('false', 'Erro ao excluir adicional do produto!', $adicionaisDesteItem[$i], 406);
                        }
                    }
                }
                $deleteResponse = $response->delete();
                if(isset($pedido) && !is_null($deleteResponse)){
                    $payload['TOTAL'] = !is_null($pedido->TOTAL) ? $pedido->TOTAL - $totalPedidoItem - $totalAdicionais > 0 ? $pedido->TOTAL - $totalPedidoItem - $totalAdicionais : 0 : 0;
                    $pedido->fill($payload);
                    $updatePedido = $pedido->save();                
                    $deleteResponse = $totalAdicionais
                        ? array(
                            'totalAdicionais' => $totalAdicionais,
                            'pedidoResponse' => $updatePedido,
                            'pedidoItemResponse' => $deleteResponse )
                        :  array(
                            'pedidoResponse' => $updatePedido,
                            'pedidoItemResponse' => $deleteResponse );
                    if(!$updatePedido){
                        DB::rollback();
                        return Handles::jsonResponse('false', 'Erro ao atualizar o pedido vinculado!', $deleteResponse, 408);
                    }
                }
            }else{
                $deleteResponse = $response->delete();
            }
            DB::commit();
            return Handles::jsonResponse('true', 'Registro excluído', $deleteResponse, 200);
        } catch (\Illuminate\Database\QueryException $exception) {
            DB::rollback();
            // You can check get the details of the error using `errorInfo`:
            return Handles::jsonResponse('false', 'Record not deleted -  - Validation exception!', $exception, 405);
        }
    }
}
