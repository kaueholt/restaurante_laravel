<?php

namespace App\Http\Controllers;

use App\Models\pedidos;
use App\Models\pedidospesquisas;
use App\Models\pedidosstatus;
use App\Models\restaurantesmesas;
use Illuminate\Http\Request;
use App\Http\Controllers\Utils\Handles;
use Illuminate\Support\Facades\DB;

class RestaurantesController extends Controller
{

    /**
     * @OA\Get(
     *     path="/api/v1/restaurante/{id}",
     *     tags={"Custom"},
     *     summary="Detalhes do restaurante",
     *     description="Detalhes do restaurante",
     *     @OA\Parameter(
     *         name="cidade",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="CURITIBA"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=172
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    public function showRestaurante($idRestaurante){
        $sis_url = env('SISTEMA_URL');
        // $sql  = "SELECT P.CODIGO, I.IDANUNCIO, I.DESTAQUE, IF( (I.ID <> '' and I.ID is not null), CONCAT('".$adm_url."','/images/doc/',I.MODULO,'/',I.ID,'.jpg'), '') as LINK ";
        $sql  = "SELECT P.*, 
            IF( (I.ID <> '' and I.ID is not null), CONCAT('".$sis_url."','/images/doc/',LOWER(I.MODULO),'/',I.ID_DOC,'/',I.ID,'.jpg'), '') as LINK, ";
        $sql .= "IF(FIND_IN_SET(WEEKDAY(CURDATE()), P.DIASEMANA) && ( ";
        $sql .= "(WEEKDAY(CURDATE()) < 5 && ((CURRENT_TIME() > P.INICIOMANHA && CURRENT_TIME() < P.FIMMANHA) || ";
        $sql .= "(CURRENT_TIME() > P.INICIOTARDE && CURRENT_TIME() < P.FIMTARDE))) || ";
        $sql .= "(WEEKDAY(CURDATE()) = 5 && ((CURRENT_TIME() > P.INICIOSABADOMANHA && CURRENT_TIME() < P.FIMSABADOMANHA) || ";
        $sql .= "(CURRENT_TIME() > P.INICIOSABADOTARDE && CURRENT_TIME() < P.FIMSABADOTARDE))) || ";
        $sql .= "(WEEKDAY(CURDATE()) = 6 && ((CURRENT_TIME() > P.INICIODOMINGOMANHA && CURRENT_TIME() < P.FIMDOMINGOMANHA) || ";
        $sql .= "(CURRENT_TIME() > P.INICIODOMINGOTARDE && CURRENT_TIME() < P.FIMDOMINGOTARDE)))),true,false) as ABERTO ";
        $sql .= "FROM CRM_PESSOA P ";
        $sql .= "LEFT JOIN AMB_IMAGEM I ON P.ID=I.ID_DOC AND LOWER(I.MODULO) = 'crm' ";
        $sql .= "WHERE P.PESSOA = 'R' ";
        $sql .= "AND P.ID = $idRestaurante LIMIT 1";
         
        $response = DB::select($sql);
        if ($response && isset($response[0])) {
            unset($response[0]->PASSWORD);
            unset($response[0]->SENHA);
            unset($response[0]->USERLOGIN);
            unset($response[0]->EMAIL);
            unset($response[0]->EMAILSENHA);
        }
        return $response;
    }
    /**
     * @OA\Get(
     *     path="/api/v1/restaurantes/{id}/mesa/{idMesa}",
     *     tags={"Custom"},
     *     summary="Lista detalhes da mesa específica {idMesa} com pedido EM ABERTO do restaurante {id}",
     *     description="Retorna os detalhes da mesa específica com pedido EM ABERTO {idMesa} do restaurante {id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="idMesa",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showPedidoMesa($idRestaurante,$idMesa){
        return pedidos::select('FAT_PEDIDO.ID','CRM_PESSOA.NOME','CRM_PESSOA.ID AS CLIENTE_ID')
            ->leftjoin('CRM_PESSOA', 'FAT_PEDIDO.ID_CLIENTE','CRM_PESSOA.ID')
            ->where('FAT_PEDIDO.ID_RESTAURANTE',$idRestaurante)
            ->where('FAT_PEDIDO.ID_MESA',$idMesa)
            ->whereNull('DATAFECHAMENTO')
            ->where('SITUACAO','A')
            ->orderBy('NOME','ASC')
            ->groupBy('CLIENTE_ID','CRM_PESSOA.NOME','FAT_PEDIDO.ID')
            ->get();
        // return pedidos::select('FAT_PEDIDO.*','CRM_PESSOA.NOME','FAT_PEDIDO_ITEM.NRITEM','FAT_PEDIDO_ITEM.ID_PRODUTO_RESTAURANTE',
        //         'FAT_PEDIDO_ITEM.QTSOLICITADA','FAT_PEDIDO_ITEM.QTATENDIDA','FAT_PEDIDO_ITEM.VALORUNITARIO','FAT_PEDIDO_ITEM.VALORDESCONTO',
        //         'FAT_PEDIDO_ITEM.VALORTOTAL','FAT_PEDIDO_ITEM_ADICIONAL.VALOR','FAT_PEDIDO_ITEM_ADICIONAL.QUANTIDADE',
        //         'FAT_PEDIDO_PESQUISA.ID_PEDIDO','FAT_PEDIDO_PESQUISA.CREATED_AT',
        //         'FAT_PEDIDO_PESQUISA.UPDATED_AT','S1.PERGUNTA AS PERGUNTA1','FAT_PEDIDO_PESQUISA.RESPOSTA1',
        //         'S2.PERGUNTA AS PERGUNTA2','FAT_PEDIDO_PESQUISA.RESPOSTA2','S3.PERGUNTA AS PERGUNTA3',
        //         'FAT_PEDIDO_PESQUISA.RESPOSTA3','FAT_PEDIDO_PESQUISA.MEDIA')
        //     ->leftjoin('FAT_PEDIDO_ITEM', 'FAT_PEDIDO_ITEM.PEDIDO_ID','FAT_PEDIDO.ID')
        //     ->leftjoin('FAT_PEDIDO_ITEM_ADICIONAL', 'FAT_PEDIDO_ITEM.ID','FAT_PEDIDO_ITEM_ADICIONAL.ID_PEDIDO_ITEM')
        //     ->leftjoin('CRM_PESSOA', 'FAT_PEDIDO.ID_CLIENTE','CRM_PESSOA.ID')
        //     ->leftjoin('FAT_PEDIDO_PESQUISA', 'FAT_PEDIDO_PESQUISA.ID_PEDIDO','FAT_PEDIDO.ID')
        //     ->leftjoin('FAT_PERGUNTAS_STATUS as S1','FAT_PEDIDO_PESQUISA.PERGUNTA1','S1.ID')
        //     ->leftjoin('FAT_PERGUNTAS_STATUS as S2','FAT_PEDIDO_PESQUISA.PERGUNTA2','S2.ID')
        //     ->leftjoin('FAT_PERGUNTAS_STATUS as S3','FAT_PEDIDO_PESQUISA.PERGUNTA3','S3.ID')
        //     ->where('FAT_PEDIDO.ID_RESTAURANTE',$idRestaurante)
        //     ->where('FAT_PEDIDO.ID_MESA',$idMesa)
        //     ->whereNull('DATAFECHAMENTO')
        //     ->where('SITUACAO','A')
        //     ->get();
    }
    /**
     * @OA\Get(
     *     path="/api/v1/restaurantes/{id}",
     *     tags={"Custom"},
     *     summary="Lista detalhes da mesa específica {idMesa} do restaurante {id}",
     *     description="Retorna os detalhes da mesa específica {idMesa} do restaurante {id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */
    public function showRestauranteMesas($idRestaurante){
        $response = restaurantesmesas::where('ID_RESTAURANTE',$idRestaurante)->orderby('ID','ASC')->get();
        return Handles::jsonResponse('true', 'Record list!', $response);
    }
    /**
     * @OA\Put(
     *     path="/api/v1/restaurantes/garcom",
     *     tags={"Custom"},
     *     summary="Alterna o booleano Chama_Garcom da mesa específica {ID_MESA} do restaurante {ID_RESTAURANTE}, com o usuário update {ID_PESSOA}",
     *     description="Alterna o booleano Chama_Garcom da mesa específica {ID_MESA} do restaurante {ID_RESTAURANTE}, com o usuário update {ID_PESSOA}",
     *     @OA\Parameter(
     *         name="ID_MESA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_RESTAURANTE",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PESSOA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function toogleChamaGarcom(Request $request){
        $payload = $request->all();
        $idRestaurante = $payload['ID_RESTAURANTE'];
        $idMesa = $payload['ID_MESA'];
        $idPessoa = $payload['ID_PESSOA'];
        $restaurantesMesas = restaurantesmesas::where('ID_RESTAURANTE',$idRestaurante)
            ->where('ID',$idMesa)
            ->orderby('ID','ASC')
            ->first();
        if(!$restaurantesMesas){
            return Handles::jsonResponse('false', 'Mesa não encontrada!', $restaurantesMesas);
        }
        $newRestauranteMesas['ID'] = $restaurantesMesas['ID'];
        $newRestauranteMesas['ID_RESTAURANTE'] = $restaurantesMesas['ID_RESTAURANTE'];
        $newRestauranteMesas['DESCRICAO'] = $restaurantesMesas['DESCRICAO'];
        $newRestauranteMesas['LINKQRCODE'] = $restaurantesMesas['LINKQRCODE'];
        $newRestauranteMesas['CREATED_AT'] = $restaurantesMesas['CREATED_AT'];
        $newRestauranteMesas['USERINSERT'] = $restaurantesMesas['USERINSERT'];
        $newRestauranteMesas['UPDATED_AT'] = date('Y-m-d H:i:s');
        $newRestauranteMesas['USERUPDATE'] = $idPessoa;
        $newRestauranteMesas['CHAMA_GARCOM'] = $restaurantesMesas['CHAMA_GARCOM'] ? 0 : 1;
        try{
            $restaurantesMesas->fill($newRestauranteMesas);
            $response = $restaurantesMesas->save();
            $response = restaurantesmesas::where('ID_RESTAURANTE',$idRestaurante)
            ->where('ID',$idMesa)
            ->orderby('ID','ASC')
            ->first();
            return Handles::jsonResponse('true', ($newRestauranteMesas['CHAMA_GARCOM'] ? 'Garçom solicitado!' : 'Garçom dispensado!' ), $response);
        }catch (\Illuminate\Database\QueryException $exception) {
            return Handles::jsonResponse('false', 'ERROR! EXPECTED PARAMS: ID, ID_RESTAURANTE, ID_MESA, ID_PESSOA, DESCRICAO, LINKQRCODE, CREATED_AT, USERINSERT, CHAMA_GARCOM', $exception);
        }
    }
    /**
     * @OA\Get(
     *     path="/api/v1/restaurantes/cupons/disponiveis/{id}",
     *     tags={"Custom"},
     *     summary="Lista todos os cupons que foram inseridos pelo administrador ou pelo próprio restaurante {id}. Finalizar essa API quando a tabela EST_RESTAURANTE_PESSOA for criada.",
     *     description="Retorna uma lista com todos os CUPONS que podem ser vinculados ao restaurante {id}",
     *     @OA\Parameter(
     *         name="id",
     *          description="ID do RESTAURANTE",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=4
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showCuponsDisponiveis($id){
        $sql = "SELECT C.*
                FROM FAT_CUPOM C
                RIGHT JOIN CRM_PESSOA P 
                    ON C.USERINSERT = P.ID
                WHERE ((C.VALIDADEINICIO <= NOW()) 
                    AND (C.VALIDADEFIM >= NOW()))
                    AND (P.PESSOA = 'A' OR P.PESSOA = 'R')";    
        $response = DB::select($sql);
        return Handles::jsonResponse('true', 'Record list!', $response);
    }
    /**
     * @OA\Get(
     *     path="/api/v1/restaurantes/cupons/{id}",
     *     tags={"Custom"},
     *     summary="Lista todos os cupons que foram vinculados ao restaurante {id}",
     *     description="Retorna uma lista com todos os CUPONS que foram vinculados ao restaurante {id}",
     *     @OA\Parameter(
     *         name="id",
     *          description="id do RESTAURANTE",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=4
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Record list!",
     *     ),
     * )
    */

    public function showCuponsRestaurante($id){
        $sql =  "SELECT P.ID AS CUPOMPESSOA_ID, C.ID AS CUPOM_ID, PE.ID AS PESSOA_ID, C.DESCRICAO, C.VALOR, C.VALIDADEINICIO, C.VALIDADEFIM, P.UTILIZADO, ";
        $sql .= "IF(((C.VALIDADEINICIO <= CURDATE()) and (C.VALIDADEFIM >= CURDATE()) and (P.UTILIZADO = false)), 'Ativo', 'Inativo') as STATUS ";
        $sql .= "FROM FAT_CUPOM_PESSOA P ";
        $sql .= "RIGHT JOIN FAT_CUPOM C ON C.ID = P.ID_CUPOM ";
        $sql .= "RIGHT JOIN CRM_PESSOA PE ON P.ID_PESSOA = PE.ID AND PE.PESSOA = 'R' ";
        $sql .= "WHERE P.ID_PESSOA = ". $id;
        $response = DB::select($sql);
        return Handles::jsonResponse('true', 'Record list!', $response);
    }
}

