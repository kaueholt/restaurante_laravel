<?php
/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Mizefood Api",
 *      description="Mizefood Swagger Api description",
 *      @OA\Contact(
 *          email="marcio.sergio@admsistema.com.br"
 *      ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/atividades",
 *     summary="Retorna todos os registros da tabela atividades.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela atividades.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/classes",
 *     summary="Retorna todos os registros da tabela classes.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela classes.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/cupompessoas",
 *     summary="Retorna todos os registros da tabela cupompessoas.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela cupompessoas.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/cupons",
 *     summary="Retorna todos os registros da tabela cupons.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela cupons.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/ddm",
 *     summary="Retorna todos os registros da tabela ddm.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela ddm.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/empresas",
 *     summary="Retorna todos os registros da tabela empresas.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela empresas.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/forms",
 *     summary="Retorna todos os registros da tabela forms.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela forms.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/grupos",
 *     summary="Retorna todos os registros da tabela grupos.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela grupos.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/gruposadicionais",
 *     summary="Retorna todos os registros da tabela gruposadicionais.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela gruposadicionais.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/gruposrestaurante",
 *     summary="Retorna todos os registros da tabela gruposrestaurante.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela gruposrestaurante.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/imagens",
 *     summary="Retorna todos os registros da tabela imagens.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela imagens.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/jobs",
 *     summary="Retorna todos os registros da tabela jobs.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela jobs.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/pedidos",
 *     summary="Retorna todos os registros da tabela pedidos.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela pedidos.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/pedidositens",
 *     summary="Retorna todos os registros da tabela pedidositens.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela pedidositens.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/pedidositensadicionais",
 *     summary="Retorna todos os registros da tabela pedidositensadicionais.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela pedidositensadicionais.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/pedidospesquisas",
 *     summary="Retorna todos os registros da tabela pedidospesquisas.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela pedidospesquisas.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/pessoas",
 *     summary="Retorna todos os registros da tabela pessoas.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela pessoas.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/produtos",
 *     summary="Retorna todos os registros da tabela produtos.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela produtos.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/restaurantesmesas",
 *     summary="Retorna todos os registros da tabela restaurantesmesas.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela restaurantesmesas.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/restaurantesprodutos",
 *     summary="Retorna todos os registros da tabela restaurantesprodutos.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela restaurantesprodutos.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/usuarios",
 *     summary="Retorna todos os registros da tabela usuarios.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela usuarios.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

/**
 * @OA\Get(
 *     path="/api/v1/default/usuariosautoriza",
 *     summary="Retorna todos os registros da tabela usuariosautoriza.",
 *     tags={"Default"},
 *     description="Retorna todos os registros da tabela usuariosautoriza.",
 *     @OA\Response(
 *          response="200",
 *          description="Record list!",
 *     ),
 * )
*/

?>