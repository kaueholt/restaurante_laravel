<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Utils\Handles;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){
        auth()->setDefaultDriver('api'); 
        // any controller that extends from this controller is going to use JWT authentication
    }

    public function authUser(){
        try{
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e){ // https://youtu.be/6eX9Pj-GhZs?t=1140 - lista de erros 
            return response()->json(['success' => false, 'message' => 'Token não encontrado','user' => null]);
        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e){
            return response()->json(['success' => false, 'message' => 'Token já renovado.','user' => null]);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
            return response()->json(['success' => false, 'message' => 'Token expirado.','user' => null]);
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e){
            return response()->json(['success' => false, 'message' => 'Token não encontrado.','user' => null]);
        }
        return response()->json(['success' => true, 'message' => 'Token válido','user' => $user]);
    }
}
