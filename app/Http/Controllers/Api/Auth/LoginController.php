<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\pessoas;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\Controller;
use App\Http\Controllers\Utils\Handles;


class LoginController extends Controller
{
    public function refresh(){
        try{
            $newToken = auth()->refresh();
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e){ // https://youtu.be/6eX9Pj-GhZs?t=1140 - lista de erros 
            return response()->json(['success' => false, 'message' => 'Token não encontrado','token' => null]);
        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e){
            return response()->json(['success' => false, 'message' => 'Token já renovado.','token' => null]);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
            return response()->json(['success' => false, 'message' => 'Token expirado.','token' => null]);
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e){
            return response()->json(['success' => false, 'message' => 'Token não encontrado.','token' => null]);
        }
        return response()->json(['success' => true, 'message' => 'Token atualizado','token' => $token]);
    }    

    public function UID(String $UID){
        $emptyData['ID'] = null;
        $emptyData['NOME'] = null;
        $emptyData['AVATAR'] = null;
        $emptyData['NOMEREDUZIDO'] = null;
        $emptyData['PESSOA'] = null;

        $existeUID = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA', 'AVATAR', 'UIDPHONE')
            ->where("UID", $UID)
            ->first();
        if ($existeUID){
            return response()->json(
                array(
                    'data' => $existeUID,
                    'code' => 200,
                    'success' => true,
                    'message' => 'Bem vindo!'
                )
            );
        }
        return response()->json(
            array(
                'data' => $emptyData,
                'code' => 500,
                'success' => false,
                'message' => 'Usuário não encontrado.'
            )
        );
    }
    
    public function Login(Request $request){
        $credentials = $request->only(['EMAIL','PASSWORD']);
        // $token = auth()->attempt($credentials);
        if(!$token = auth()->attempt($credentials)){
            $credentials = $request->all();
            $emptyData['ID'] = null;
            $emptyData['NOME'] = null;
            $emptyData['NOMEREDUZIDO'] = null;
            $emptyData['PESSOA'] = null;
            if(!isset($credentials['EMAIL']) || !isset($credentials['PASSWORD'])){
                return response()->json(
                    array(
                        'data' => $emptyData,
                        'code' => 500,
                        'success' => false,
                        'message' => 'Usuário / Senha não informados.'
                    )
                );
            }
            $user = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA')
                ->where("EMAIL", $credentials['EMAIL'])
                ->where("PASSWORD", $credentials['PASSWORD'])
                ->whereNotNull('PASSWORD')
                ->where('PESSOA','C')
                ->first();
            if ($user){
                return response()->json(
                    array(
                        'data' => $user,
                        'code' => 401,
                        'success' => false,
                        'message' => 'Erro de permissão.'
                    )
                );
            }
            $existeEmail = pessoas::select('ID')
                ->where("EMAIL", $credentials['EMAIL'])
                ->first();
            if(!$existeEmail){
                return response()->json(
                    array(
                        'data' => $emptyData,
                        'code' => 500,
                        'success' => false,
                        'message' => 'E-mail não cadastrado'
                    )
                );
            }
            $isNotCliente = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA')
                ->where("EMAIL", $credentials['EMAIL'])
                ->where("PESSOA",'<>','C')
                ->first();
            if($isNotCliente){
                return response()->json(
                    array(
                        'data' => $emptyData,
                        'code' => 500,
                        'success' => false,
                        'message' => 'Usuário não é do tipo Cliente!'
                    )
                );
            }
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'Senha inválida'
                )
            );
        }
        $user = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA')
            ->where("EMAIL", $credentials['EMAIL'])
            ->where("PASSWORD", $credentials['PASSWORD'])
            ->whereNotNull('PASSWORD')
            ->where('PESSOA','C')
            ->first();
        if ($user){
            $user->token = $token;
            return response()->json(
                array(
                    'data' => $user,
                    'code' => 200,
                    'success' => true,
                    'message' => 'Bem vindo!'
                )
            );
        }
        // return response()->json(['token' => $token]);
        // return $token;
             
    }

   public function LoginGarcom(Request $request){
        $credentials = $request->all();
        $emptyData['ID'] = null;
        $emptyData['NOME'] = null;
        $emptyData['NOMEREDUZIDO'] = null;
        $emptyData['PESSOA'] = null;
        if(!isset($credentials['EMAIL']) || !isset($credentials['PASSWORD'])){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'Usuário / Senha não informados.'
                )
            );
        }
        $user = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA','ID_RESTAURANTE_GARCOM','AVATAR','UIDPHONE')
            ->where("EMAIL", $credentials['EMAIL'])
            ->where("PASSWORD", $credentials['PASSWORD'])
            ->whereNotNull('PASSWORD')
            ->whereNotNull('ID_RESTAURANTE_GARCOM')
            ->where('PESSOA','G')
            ->first();
        if ($user){
            return response()->json(
                array(
                    'data' => $user,
                    'code' => 200,
                    'success' => true,
                    'message' => 'Bem vindo!'
                )
            );
        }
        $existeEmail = pessoas::select('ID')
            ->where("EMAIL", $credentials['EMAIL'])
            ->first();
        if(!$existeEmail){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'E-mail não cadastrado'
                )
            );
        }
        $isNotGarcom = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA')
            ->where("EMAIL", $credentials['EMAIL'])
            ->where("PASSWORD", $credentials['PASSWORD'])
            ->where("PESSOA",'<>','G')
            ->first();
        if($isNotGarcom){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'Usuário não é Garçom'
                )
            );
        }
        $vinculoRestaurante = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA')
            ->where("EMAIL", $credentials['EMAIL'])
            ->where("PASSWORD", $credentials['PASSWORD'])
            ->where("PESSOA",'<>','G')
            ->first();
        if(!$vinculoRestaurante){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 501,
                    'success' => false,
                    'message' => 'Garçom sem vínculo com restaurante'
                )
            );
        }
        return response()->json(
            array(
                'data' => $emptyData,
                'code' => 500,
                'success' => false,
                'message' => 'Senha inválida'
            )
        );        
    }

    /**
     * Search FINCLIENTE with cnpjCpf and PASSWORD and format return according to admin area
     *
     * @param  varchar $cnpjCpf
     * @param  varchar $PASSWORD
     * @return \Illuminate\Http\Response
     */
    public function loginAdmin(Request $request)
    {
        // Busca pela usuario que tenha email e senha iguais 
        $user = $this->Login($request);

        // verifica resultado 
        if (!empty($user)) {
            return  response()->json(
                array(
                    'code' => 20000,
                    'data' => array('token' => 'admin-token')
                )
            );
        }
        else {
            return  response()->json(
                array(
                    'code' => 60204,
                    'message' => 'Account and PASSWORD are incorrect.'                )
            );

        }

        // retornar somete um resultado
    }


    /**
     * @param Request $mail
     * @return \Illuminate\Http\Response
     */
    public function resetPASSWORD(Request $mail)
    {
        //print_r($mail);
        $FINCLIENTE = $this->Usuario->findByEmail($mail[0]);
        $mailer = new MailRulesController();
        
        $sendMail = $mailer->sendMailResetPASSWORD($FINCLIENTE[0]->email, $FINCLIENTE[0]->id);
        //print_r($sendMail);
        return "Status envio: {$sendMail}";

    }

    /**
     * @param Request $mail
     * @return \Illuminate\Http\Response
     */
    public function changePASSWORD(Request $credentials)
    {
        //print_r($credentials);

        $sql = "update usuario ";
        $sql .= "set PASSWORD = '".$credentials->PASSWORD."'";
        $sql .= " where id = ". $credentials->userId;

        print_r($sql);

        $affected = DB::update($sql);

        return $affected;

        // // Buscar pela usuario com o id recebido
        // $FINCLIENTE = Pessoa::find($credentials->userId);
        // // Insere nova senha
        // $FINCLIENTE->PASSWORD = $credentials->PASSWORD;
        // // salva os dados, retornando 1 para sucesso e 0 para erro
        // return $FINCLIENTE->save() ? 1 : 0;
    }
}
