<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MailRulesController;
use App\Models\pessoas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserAuthController extends Controller
{
    private $Usuario;

    /**
     * UserAuthController constructor.
     */
    public function __construct()
    {
    }

    /**
     * Search FINCLIENTE with cnpjCpf and password.
     *
     * @param  varchar $cnpjCpf
     * @param  varchar $password
     * @return \Illuminate\Http\Response
     */
    public function UID(String $UID){
        $emptyData['ID'] = null;
        $emptyData['NOME'] = null;
        $emptyData['AVATAR'] = null;
        $emptyData['NOMEREDUZIDO'] = null;
        $emptyData['PESSOA'] = null;

        $existeUID = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA', 'AVATAR','UIDPHONE')
            ->where("UID", $UID)
            ->first();
        if ($existeUID){
            return response()->json(
                array(
                    'data' => $existeUID,
                    'code' => 200,
                    'success' => true,
                    'message' => 'Bem vindo!'
                )
            );
        }
        return response()->json(
            array(
                'data' => $emptyData,
                'code' => 500,
                'success' => false,
                'message' => 'Usuário não encontrado.'
            )
        );
    }

    public function email($email){
        $emptyData['ID'] = null;
        $emptyData['NOME'] = null;
        $emptyData['AVATAR'] = null;
        $emptyData['NOMEREDUZIDO'] = null;
        $emptyData['PESSOA'] = null;
        $emptyData['UID'] = null;
        $emptyData['EMAIL'] = null;

        $existeEmail = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA', 'AVATAR','UIDPHONE', 'UID', 'EMAIL')
            ->where("EMAIL", $email)
            ->first();
        if ($existeEmail){
            return response()->json(
                array(
                    'data' => $existeEmail,
                    'code' => 200,
                    'success' => true,
                    'message' => 'Bem vindo!'
                )
            );
        }
        return response()->json(
            array(
                'data' => $emptyData,
                'code' => 500,
                'success' => false,
                'message' => 'Usuário não encontrado.'
            )
        );
    }


   public function Login(Request $request){
        $payload = $request->all();
        $emptyData['ID'] = null;
        $emptyData['NOME'] = null;
        $emptyData['AVATAR'] = null;
        $emptyData['NOMEREDUZIDO'] = null;
        $emptyData['PESSOA'] = null;
        if(!isset($payload['username']) || !isset($payload['password'])){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'Usuário / Senha não informados.'
                )
            );
        }
        $user = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA', 'AVATAR','UIDPHONE')
            ->where("EMAIL", $payload['username'])
            ->where("PASSWORD", $payload['password'])
            ->whereNotNull('PASSWORD')
            ->where('PESSOA','C')
            ->first();
        if ($user){
            return response()->json(
                array(
                    'data' => $user,
                    'code' => 200,
                    'success' => true,
                    'message' => 'Bem vindo!'
                )
            );
        }
        $existeEmail = pessoas::select('ID')
            ->where("EMAIL", $payload['username'])
            ->first();
        if(!$existeEmail){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'E-mail não cadastrado'
                )
            );
        }
        $isNotCliente = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA', 'AVATAR','UIDPHONE')
            ->where("EMAIL", $payload['username'])
            ->where("PESSOA",'<>','C')
            ->first();
        if($isNotCliente){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'Usuário não é do tipo Cliente!'
                )
            );
        }
        return response()->json(
            array(
                'data' => $emptyData,
                'code' => 500,
                'success' => false,
                'message' => 'Senha inválida'
            )
        );        
    }

   public function LoginGarcom(Request $request){
        $payload = $request->all();
        $emptyData['ID'] = null;
        $emptyData['NOME'] = null;
        $emptyData['AVATAR'] = null;
        $emptyData['NOMEREDUZIDO'] = null;
        $emptyData['PESSOA'] = null;
        if(!isset($payload['username']) || !isset($payload['password'])){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'Usuário / Senha não informados.'
                )
            );
        }
        $user = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA','ID_RESTAURANTE_GARCOM', 'AVATAR','UIDPHONE')
            ->where("EMAIL", $payload['username'])
            ->where("PASSWORD", $payload['password'])
            ->whereNotNull('PASSWORD')
            ->whereNotNull('ID_RESTAURANTE_GARCOM')
            ->where('PESSOA','G')
            ->first();
        if ($user){
            return response()->json(
                array(
                    'data' => $user,
                    'code' => 200,
                    'success' => true,
                    'message' => 'Bem vindo!'
                )
            );
        }
        $existeEmail = pessoas::select('ID')
            ->where("EMAIL", $payload['username'])
            ->first();
        if(!$existeEmail){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'E-mail não cadastrado'
                )
            );
        }
        $isNotGarcom = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA', 'AVATAR','UIDPHONE')
            ->where("EMAIL", $payload['username'])
            ->where("PASSWORD", $payload['password'])
            ->where("PESSOA",'<>','G')
            ->first();
        if($isNotGarcom){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'Usuário não é Garçom'
                )
            );
        }
        $vinculoRestaurante = pessoas::select('ID', 'NOME', 'NOMEREDUZIDO', 'PESSOA', 'AVATAR','UIDPHONE')
            ->where("EMAIL", $payload['username'])
            ->where("PASSWORD", $payload['password'])
            ->where("PESSOA",'G')
            ->whereNull('ID_RESTAURANTE_GARCOM')
            ->first();
        if(!$vinculoRestaurante){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 501,
                    'success' => false,
                    'message' => 'Garçom sem vínculo com restaurante'
                )
            );
        }
        return response()->json(
            array(
                'data' => $emptyData,
                'code' => 500,
                'success' => false,
                'message' => 'Senha inválida'
            )
        );        
    }

    /**
     * Search FINCLIENTE with cnpjCpf and password and format return according to admin area
     *
     * @param  varchar $cnpjCpf
     * @param  varchar $password
     * @return \Illuminate\Http\Response
     */
    public function loginAdmin(Request $request)
    {
        // Busca pela usuario que tenha email e senha iguais 
        $user = $this->Login($request);

        // verifica resultado 
        if (!empty($user)) {
            return  response()->json(
                array(
                    'code' => 20000,
                    'data' => array('token' => 'admin-token')
                )
            );
        }
        else {
            return  response()->json(
                array(
                    'code' => 60204,
                    'message' => 'Account and password are incorrect.'                )
            );

        }

        // retornar somete um resultado
    }


    /**
     * @param Request $mail
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $mail)
    {
        //print_r($mail);
        $FINCLIENTE = $this->Usuario->findByEmail($mail[0]);
        $mailer = new MailRulesController();
        
        $sendMail = $mailer->sendMailResetPassword($FINCLIENTE[0]->email, $FINCLIENTE[0]->id);
        //print_r($sendMail);
        return "Status envio: {$sendMail}";

    }

    /**
     * @param Request $mail
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $payload)
    {
        //print_r($payload);

        $sql = "update usuario ";
        $sql .= "set password = '".$payload->password."'";
        $sql .= " where id = ". $payload->userId;

        print_r($sql);

        $affected = DB::update($sql);

        return $affected;

        // // Buscar pela usuario com o id recebido
        // $FINCLIENTE = Pessoa::find($payload->userId);
        // // Insere nova senha
        // $FINCLIENTE->password = $payload->password;
        // // salva os dados, retornando 1 para sucesso e 0 para erro
        // return $FINCLIENTE->save() ? 1 : 0;
    }
    /**
     * info user with id.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function infoUser(Request $request)
    {
        $payload = $request->all();
        $response = FINCLIENTE::select('*')
        ->where("CELULAR", $payload['celular'])
        ->first();

        return $response;
    }



}