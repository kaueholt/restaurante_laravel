<?php

/**
* @OA\Delete(
*     path="/api/v1/default/atividades/{ATIVIDADE}",
*     summary="Edita o registro com ID {ATIVIDADE} da tabela de atividades.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*      @OA\Parameter(
*         name="ATIVIDADE",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="string",
*             example=""
*         )
*     ),
* )
*/
/**
* @OA\Delete(
*     path="/api/v1/default/classes/{CLASSE}",
*     summary="Edita o registro com ID {CLASSE} da tabela de classes.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*      @OA\Parameter(
*         name="CLASSE",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="string",
*             example=""
*         )
*     ),
* )
*/
/**
* @OA\Delete(
*     path="/api/v1/default/cupompessoas/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de cupompessoas.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/
/**
* @OA\Delete(
*     path="/api/v1/default/cupons/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de cupons.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/

/**
* @OA\Delete(
*     path="/api/v1/default/ddm/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de ddm.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/
/**
* @OA\Delete(
*     path="/api/v1/default/forms/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de form.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/
/**
* @OA\Delete(
*     path="/api/v1/default/empresas/{EMPRESA}",
*     summary="Edita o registro com ID {EMPRESA} da tabela de empresas.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="EMPRESA",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/
/**
* @OA\Delete(
*     path="/api/v1/default/grupos/{GRUPO}",
*     summary="Edita o registro com ID {GRUPO} da tabela de grupos.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="GRUPO",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="string",
*             example=""
*         )
*     ),
* )
*/

/**
* @OA\Delete(
*     path="/api/v1/default/gruposadicionais/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de gruposadicionais.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/

/**
* @OA\Delete(
*     path="/api/v1/default/gruposrestaurante/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de gruposrestaurante.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/

/**
* @OA\Delete(
*     path="/api/v1/default/imagens/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de imagens.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/
/**
* @OA\Delete(
*     path="/api/v1/default/jobs/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de jobs.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
*
* )
*/
/**
* @OA\Delete(
*     path="/api/v1/default/pedidos/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de pedidos.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/

/**
* @OA\Delete(
*     path="/api/v1/default/pedidositens/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de pedidositens.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=1
*         )
*     ),
* )
*/

/**
* @OA\Delete(
*     path="/api/v1/default/pedidositensadicionais/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de pedidositensadicionais.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/

/**
* @OA\Delete(
*     path="/api/v1/default/pedidospesquisas/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de pedidospesquisas.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/

/**
* @OA\Delete(
*     path="/api/v1/default/pessoas/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de pessoas.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/
/**
* @OA\Delete(
*     path="/api/v1/default/produtos/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de produtos.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/

/**
* @OA\Delete(
*     path="/api/v1/default/restaurantesmesas/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de restaurantesmesas.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/

/**
* @OA\Delete(
*     path="/api/v1/default/restaurantesprodutos/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de restaurantesprodutos.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/

/**
* @OA\Delete(
*     path="/api/v1/default/usuarios/{USUARIO}",
*     summary="Edita o registro com ID {USUARIO} da tabela de usuarios.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*      @OA\Parameter(
*         name="USUARIO",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/

/**
* @OA\Delete(
*     path="/api/v1/default/usuariosautoriza/{ID}",
*     summary="Edita o registro com ID {ID} da tabela de usuariosautoriza.",
*     description="Apenas os campos enviados são alterados.",
*     tags={"Default"},
*     @OA\Response(
*         response=201,
*         description="Registro atualizado!"
*     ),
*     @OA\Response(
*         response=422,
*         description="Falha ao atualizar registro - Dados faltantes"
*     ),
*     @OA\Response(
*         response=421,
*         description="Registro não atualizado - Chave existente"
*     ),
*     @OA\Parameter(
*         name="ID",
*         in="path",
*         required=true,
*         @OA\Schema(
*             type="integer",
*             example=""
*         )
*     ),
* )
*/

?>
