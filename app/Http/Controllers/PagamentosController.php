<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Utils\Handles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PagamentosController extends Controller{
  
  public function cobrancaDireta(Request $request, $id){
    $payload = $request->all();
    $pedido = DB::table('FAT_PEDIDO')->where('ID',$id)->first();
    if(!$pedido){
      return Handles::jsonResponse('false', 'Pedido não encontrado!', $payload);
    }
    $totalPedidoCentavos = $pedido->TOTAL*100;
    $itensPedido = DB::table('FAT_PEDIDO_ITEM')->where('PEDIDO_ID',$id)->get();
    if(!$itensPedido){
      return Handles::jsonResponse('false', 'Não foram encontrados itens desse pedido!', $pedido);
    }
    $itensIugu = [];
    $totalPedidoItensCentavos = 0;
    for ($i=0; $i<count($itensPedido); $i++) {
      $itensIugu[$i]['price_cents'] = intval(round($itensPedido[$i]->VALORUNITARIO*100,0));
      $itensIugu[$i]['quantity'] = intval(round($itensPedido[$i]->QTSOLICITADA,0));
      $itensIugu[$i]['description'] = $itensPedido[$i]->DESCRICAO;
      $totalPedidoItensCentavos += $itensIugu[$i]['price_cents'] * $itensIugu[$i]['quantity'];
    }
    if( (($totalPedidoItensCentavos - $totalPedidoCentavos) > 1) || (($totalPedidoItensCentavos - $totalPedidoCentavos) < -1) ){
      return Handles::jsonResponse('false', 'A somatória dos itens está diferente da somatória do pedido!', $itensIugu);
    }
    if(isset($payload['ID_CUPOM_PESSOA'])){
      $cupom = $this->showCupomPessoa($payload['ID_CUPOM_PESSOA'])->getData();
      if($cupom->data[0]->ID_PESSOA != $pedido->ID_CLIENTE){
        return Handles::jsonResponse('false', 'O cupom informado não é do cliente deste pedido!', $pedido);
      }
      if(!$cupom->data[0]->STATUS){
        return Handles::jsonResponse('false', 'O cupom informado não está mais disponível', $pedido);
      }
      $payload['discount_cents'] = $cupom->data[0]->VALOR * 100;
    }
    
    $payload['items'] = $itensIugu;
    // var_dump(intval(gettype($payload['items'][0]['quantity']));
    // var_dump(intval(round($payload['items'][0]['quantity'],0)));
    /* https://dev.iugu.com/reference#cobranca-direta */    
    $jsonString = json_encode($payload);
    if(strlen($jsonString) > 2 && $this->isJson($jsonString)){ //json string > "{}" (objeto vazio) e tb é json
      $ch = curl_init('https://api.iugu.com/v1/charge');
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
      curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonString);                                                                  
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
          'Content-Type: application/json',                                                                                
          'Content-Length: ' . strlen($jsonString))
        );
      $res = curl_exec($ch);
      curl_close($ch);
    }else{
      $res = '{"errors": "Parâmetros inválidos!"}';
    }
    $iugu = json_decode($res);
    // var_dump($iugu);
    $pedido = 
      isset($iugu->success) ? 
        $iugu->success 
        ? $pedido = DB::table('FAT_PEDIDO')->where('ID',$id)->update(
          [
            'INVOICE_IUGU'=> $iugu->invoice_id,
            'SITUACAO'=> 'B',
            'DATAFECHAMENTO'=> date("Y-m-d H:i:s"),
            'TOTAL'=> number_format(($totalPedidoItensCentavos - ( isset($payload['discount_cents']) ? $payload['discount_cents'] : 0 ))/100, 2, '.',''),
            'ID_CUPOM_PESSOA' => $payload['discount_cents'] ? $payload['ID_CUPOM_PESSOA'] : null,
            'VALORCUPOM' => $payload['discount_cents'] ? number_format((isset($payload['discount_cents']) ? $payload['discount_cents'] : 0 )/100, 2, '.','') : null,
          ])
        : false
      : false;
    if(isset($payload['discount_cents'])){
      $cupom = DB::table('FAT_CUPOM_PESSOA')->where('ID',$payload['ID_CUPOM_PESSOA'])->update(
        [
          'UTILIZADO'=> 1
        ]
      );
    }
    return $pedido
        ? Handles::jsonResponse('true', 'Record list!', array('iugu' => $iugu, 'pedido' => $id, 'pedidoUpdate' => ($pedido ? 'true' : 'false')))
        : Handles::jsonResponse('false', 'Erro de comunicação com a plataforma de pagamentos.' , $iugu);
  }
  
  public function showCupomPessoa($id){
    $sql =  "SELECT 
                C.VALOR,
                P.ID_PESSOA,
                IF(((C.VALIDADEINICIO <= CURDATE()) AND (C.VALIDADEFIM >= CURDATE()) AND (P.UTILIZADO = FALSE)), TRUE, FALSE) as STATUS
              FROM FAT_CUPOM_PESSOA P
              LEFT JOIN FAT_CUPOM C
                ON C.ID = P.ID_CUPOM
              WHERE P.ID = $id
              LIMIT 1";
    $response = DB::select($sql);
    return Handles::jsonResponse(true, 'Registro Encontrado', $response);
  }

  function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
  }
  
}