<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FATPERGUNTASSTATUS
 *
 * @property int $ID
 * @property int $PERGUNTA
 *
 * @package App\Models
 */
class perguntasstatus extends Eloquent
{
	protected $table = 'FAT_PERGUNTAS_STATUS';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'PERGUNTA'
	];

}
