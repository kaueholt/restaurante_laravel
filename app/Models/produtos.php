<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ESTPRODUTO
 *
 * @property int $ID
 * @property string $DESCRICAO
 * @property string $GRUPO
 * @property string $UNIDADE
 * @property float $PRECOVENDA
 * @property string $OBS
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @property \App\Models\ESTGRUPO $e_s_t_g_r_u_p_o
 * @property \Illuminate\Database\Eloquent\Collection $e_s_t__g_r_u_p_o__a_d_i_c_i_o_n_a_l_s
 * @property \Illuminate\Database\Eloquent\Collection $e_s_t__p_r_o_d_u_t_o__r_e_s_t_a_u_r_a_n_t_e_s
 * @property \Illuminate\Database\Eloquent\Collection $f_a_t__p_e_d_i_d_o__i_t_e_m__a_d_i_c_i_o_n_a_l_s
 *
 * @package App\Models
 */
class produtos extends Eloquent
{
	protected $table = 'EST_PRODUTO';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'PRECOVENDA' => 'float',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'DESCRICAO',
		'GRUPO',
		'UNIDADE',
		'PRECOVENDA',
		'OBS',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];

	public function e_s_t_g_r_u_p_o()
	{
		return $this->belongsTo(\App\Models\ESTGRUPO::class, 'GRUPO');
	}

	public function e_s_t__g_r_u_p_o__a_d_i_c_i_o_n_a_l_s()
	{
		return $this->hasMany(\App\Models\ESTGRUPOADICIONAL::class, 'ID_PRODUTO');
	}

	public function e_s_t__p_r_o_d_u_t_o__r_e_s_t_a_u_r_a_n_t_e_s()
	{
		return $this->hasMany(\App\Models\ESTPRODUTORESTAURANTE::class, 'ID_PRODUTO');
	}

	public function f_a_t__p_e_d_i_d_o__i_t_e_m__a_d_i_c_i_o_n_a_l_s()
	{
		return $this->hasMany(\App\Models\FATPEDIDOITEMADICIONAL::class, 'ID_PRODUTO');
	}
}
