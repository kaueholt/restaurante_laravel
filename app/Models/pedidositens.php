<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FATPEDIDOITEM
 *
 * @property int $ID
 * @property int $PEDIDO_ID
 * @property int $ID_PEDIDO_ITEM_PAI
 * @property int $NRITEM
 * @property int $ID_PRODUTO_RESTAURANTE
 * @property int $ID_PRODUTO_RESTAURANTE_PAI
 * @property float $QTSOLICITADA
 * @property float $QTATENDIDA
 * @property \Carbon\Carbon $ATENDIMENTO
 * @property float $VALORUNITARIO
 * @property float $VALORDESCONTO
 * @property float $VALORTOTAL
 * @property string $OBSERVACAO
 * @property string $DESCRICAO
 * @property int $STATUS
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @property \App\Models\ESTPRODUTORESTAURANTE $e_s_t_p_r_o_d_u_t_o_r_e_s_t_a_u_r_a_n_t_e
 * @property \App\Models\FATPEDIDO $f_a_t_p_e_d_i_d_o
 * @property \Illuminate\Database\Eloquent\Collection $f_a_t__p_e_d_i_d_o__i_t_e_m__a_d_i_c_i_o_n_a_l_s
 * @property \Illuminate\Database\Eloquent\Collection $f_i_n__j_o_b__s_t_a_t_u_s
 *
 * @package App\Models
 */
class pedidositens extends Eloquent
{
	protected $table = 'FAT_PEDIDO_ITEM';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'PEDIDO_ID' => 'int',
		'NRITEM' => 'int',
		'ID_PRODUTO_RESTAURANTE' => 'int',
		'ID_PEDIDO_ITEM_PAI' => 'int',
		'QTSOLICITADA' => 'float',
		'QTATENDIDA' => 'float',
		'VALORUNITARIO' => 'float',
		'VALORDESCONTO' => 'float',
		'VALORTOTAL' => 'float',
		'STATUS' => 'int',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'ATENDIMENTO',
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'PEDIDO_ID',
		'NRITEM',
		'ID_PRODUTO_RESTAURANTE',
		'ID_PEDIDO_ITEM_PAI',
		'QTSOLICITADA',
		'QTATENDIDA',
		'ATENDIMENTO',
		'VALORUNITARIO',
		'VALORDESCONTO',
		'VALORTOTAL',
		'DESCRICAO',
		'OBSERVACAO',
		'STATUS',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];

	public function e_s_t_p_r_o_d_u_t_o_r_e_s_t_a_u_r_a_n_t_e()
	{
		return $this->belongsTo(\App\Models\ESTPRODUTORESTAURANTE::class, 'ID_PRODUTO_RESTAURANTE');
	}

	public function f_a_t_p_e_d_i_d_o()
	{
		return $this->belongsTo(\App\Models\FATPEDIDO::class, 'PEDIDO_ID');
	}

	public function f_a_t__p_e_d_i_d_o__i_t_e_m__a_d_i_c_i_o_n_a_l_s()
	{
		return $this->hasMany(\App\Models\FATPEDIDOITEMADICIONAL::class, 'ID_PEDIDO_ITEM');
	}

	public function f_i_n__j_o_b__s_t_a_t_u_s()
	{
		return $this->hasMany(\App\Models\FINJOBSTATU::class, 'ID_JOB');
	}
}
