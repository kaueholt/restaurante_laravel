<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AMBUSUARIOAUTORIZA
 *
 * @property int $ID
 * @property int $USUARIO
 * @property string $PROGRAMA
 * @property string $DIREITOS
 *
 * @property \App\Models\AMBUSUARIO $a_m_b_u_s_u_a_r_i_o
 *
 * @package App\Models
 */
class usuariosautoriza extends Eloquent
{
	protected $table = 'AMB_USUARIO_AUTORIZA';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PESSOA' => 'int'
	];

	protected $fillable = [
		'ID_PESSOA',
		'PROGRAMA',
		'DIREITOS'
	];

	public function a_m_b_u_s_u_a_r_i_o()
	{
		return $this->belongsTo(\App\Models\AMBUSUARIO::class, 'USUARIO');
	}
}
