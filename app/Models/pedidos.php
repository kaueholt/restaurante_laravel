<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FATPEDIDO
 *
 * @property int $ID
 * @property int $ID_CLIENTE
 * @property int $ID_MESA
 * @property int $ID_RESTAURANTE
 * @property int $ID_CUPOM_PESSOA
 * @property string $SITUACAO
 * @property string $INVOICE_IUGU
 * @property \Carbon\Carbon $EMISSAO
 * @property float $TOTAL
 * @property \Carbon\Carbon $DATAFECHAMENTO
 * @property float $PERCDESCONTO
 * @property float $VALORCUPOM
 * @property string $OBS
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @property \App\Models\CRMPESSOA $c_r_m_p_e_s_s_o_a
 * @property \App\Models\FATCUPOMPESSOA $f_a_t_c_u_p_o_m_p_e_s_s_o_a
 * @property \App\Models\ESTRESTAURANTEMESA $e_s_t_r_e_s_t_a_u_r_a_n_t_e_m_e_s_a
 * @property \Illuminate\Database\Eloquent\Collection $f_a_t__p_e_d_i_d_o__i_t_e_m_s
 * @property \Illuminate\Database\Eloquent\Collection $f_a_t__p_e_d_i_d_o__p_e_s_q_u_i_s_a_s
 *
 * @package App\Models
 */
class pedidos extends Eloquent
{
	protected $table = 'FAT_PEDIDO';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_CLIENTE' => 'int',
		'ID_MESA' => 'int',
		'ID_RESTAURANTE' => 'int',
		'ID_CUPOM_PESSOA' => 'int',
		'TOTAL' => 'float',
		'PERCDESCONTO' => 'float',
		'VALORCUPOM' => 'float',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'EMISSAO',
		'DATAFECHAMENTO',
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_CLIENTE',
		'ID_MESA',
		'ID_RESTAURANTE',
		'ID_CUPOM_PESSOA',
		'SITUACAO',
		'EMISSAO',
		'TOTAL',
		'INVOICE_IUGU',
		'DATAFECHAMENTO',
		'PERCDESCONTO',
		'VALORCUPOM',
		'OBS',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(\App\Models\CRMPESSOA::class, 'ID_CLIENTE');
	}

	public function f_a_t_c_u_p_o_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(\App\Models\FATCUPOMPESSOA::class, 'ID_CUPOM_PESSOA');
	}

	public function e_s_t_r_e_s_t_a_u_r_a_n_t_e_m_e_s_a()
	{
		return $this->belongsTo(\App\Models\ESTRESTAURANTEMESA::class, 'ID_MESA');
	}

	public function f_a_t__p_e_d_i_d_o__i_t_e_m_s()
	{
		return $this->hasMany(\App\Models\FATPEDIDOITEM::class, 'PEDIDO_ID');
	}

	public function f_a_t__p_e_d_i_d_o__p_e_s_q_u_i_s_a_s()
	{
		return $this->hasMany(\App\Models\FATPEDIDOPESQUISA::class, 'ID_PEDIDO');
	}
}
