<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AMBIMAGEM
 *
 * @property int $ID
 * @property int $ID_DOC
 * @property string $MODULO
 * @property string $DESTAQUE
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @package App\Models
 */
class imagens extends Eloquent
{
	protected $table = 'AMB_IMAGEM';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_DOC' => 'int',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_DOC',
		'MODULO',
		'DESTAQUE',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];
}
