<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FINJOBSTATU
 *
 * @property int $ID
 * @property int $ID_JOB
 * @property int $VALORANTIGO
 * @property int $VALORNOVO
 * @property \Carbon\Carbon $DATAALTERACAO
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @property \App\Models\FATPEDIDOITEM $f_a_t_p_e_d_i_d_o_i_t_e_m
 *
 * @package App\Models
 */
class jobs extends Eloquent
{
	public $primaryKey = 'ID';
	protected $table = 'FIN_JOB_STATUS';
	public $incrementing = true;
	public $timestamps = false;

	protected $casts = [
		'ID_JOB' => 'int',
		'VALORANTIGO' => 'int',
		'VALORNOVO' => 'int',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'DATAALTERACAO',
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_JOB',
		'VALORANTIGO',
		'VALORNOVO',
		'DATAALTERACAO',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];

	public function f_a_t_p_e_d_i_d_o_i_t_e_m()
	{
		return $this->belongsTo(\App\Models\FATPEDIDOITEM::class, 'ID_JOB');
	}
}
