<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ESTPRODUTORESTAURANTE
 *
 * @property int $ID
 * @property int $ID_PRODUTO
 * @property int $ID_RESTAURANTE
 * @property string $ATIVO
 * @property float $PRECOPROMOCAO
 * @property \Carbon\Carbon $INICIOPROMOCAO
 * @property \Carbon\Carbon $FIMPROMOCAO
 * @property float $PRECOVENDA
 * @property string $OBS
 * @property string $IMPRESSORA
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @property \App\Models\ESTPRODUTO $e_s_t_p_r_o_d_u_t_o
 * @property \App\Models\CRMPESSOA $c_r_m_p_e_s_s_o_a
 * @property \Illuminate\Database\Eloquent\Collection $f_a_t__p_e_d_i_d_o__i_t_e_m_s
 *
 * @package App\Models
 */
class restaurantesprodutos extends Eloquent
{
	protected $table = 'EST_PRODUTO_RESTAURANTE';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PRODUTO' => 'int',
		'ID_RESTAURANTE' => 'int',
		'PRECOPROMOCAO' => 'float',
		'PRECOVENDA' => 'float',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'INICIOPROMOCAO',
		'FIMPROMOCAO',
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PRODUTO',
		'ID_RESTAURANTE',
		'ATIVO',
		'PRECOPROMOCAO',
		'INICIOPROMOCAO',
		'FIMPROMOCAO',
		'PRECOVENDA',
		'OBS',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
        'USERUPDATE',
        'IMPRESSORA'
	];

	public function e_s_t_p_r_o_d_u_t_o()
	{
		return $this->belongsTo(\App\Models\ESTPRODUTO::class, 'ID_PRODUTO');
	}

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(\App\Models\CRMPESSOA::class, 'ID_RESTAURANTE');
	}

	public function f_a_t__p_e_d_i_d_o__i_t_e_m_s()
	{
		return $this->hasMany(\App\Models\FATPEDIDOITEM::class, 'ID_PRODUTO_RESTAURANTE');
	}
}
