<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ESTGRUPO
 *
 * @property string $GRUPO
 * @property string $DESCRICAO
 * @property string $TIPO
 * @property int $NIVEL
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 * @property string $GRUPOPAI
 *
 * @property \Illuminate\Database\Eloquent\Collection $e_s_t__g_r_u_p_o__a_d_i_c_i_o_n_a_l_s
 * @property \Illuminate\Database\Eloquent\Collection $e_s_t__g_r_u_p_o__r_e_s_t_a_u_r_a_n_t_e_s
 * @property \Illuminate\Database\Eloquent\Collection $e_s_t__p_r_o_d_u_t_o_s
 *
 * @package App\Models
 */
class grupos extends Eloquent
{
	protected $table = 'EST_GRUPO';
	public $primaryKey = 'GRUPO';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'NIVEL' => 'int',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'GRUPO',
		'DESCRICAO',
		'TIPO',
		'NIVEL',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE',
		'GRUPOPAI'
	];

	public function e_s_t__g_r_u_p_o__a_d_i_c_i_o_n_a_l_s()
	{
		return $this->hasMany(\App\Models\ESTGRUPOADICIONAL::class, 'GRUPO');
	}

	public function e_s_t__g_r_u_p_o__r_e_s_t_a_u_r_a_n_t_e_s()
	{
		return $this->hasMany(\App\Models\ESTGRUPORESTAURANTE::class, 'GRUPO');
	}

	public function e_s_t__p_r_o_d_u_t_o_s()
	{
		return $this->hasMany(\App\Models\ESTPRODUTO::class, 'GRUPO');
	}
}
