<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class produtosrestaurantesadicionais
 *
 * @property int $ID
 * @property int $ID_RESTAURANTE
 * @property int $ID_PRODUTO_RESTAURANTE
 * @property int $ID_PRODUTO_RESTAURANTE_ADICIONAL
 * @property float $PRECOVENDA
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @package App\Models
 */
class produtosrestaurantesadicionais extends Eloquent
{
	protected $table = 'EST_PRODUTO_RESTAURANTE_ADICIONAL';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_RESTAURANTE' => 'int',
		'ID_PRODUTO_RESTAURANTE' => 'int',
		'ID_PRODUTO_RESTAURANTE_ADICIONAL' => 'int',
		'PRECOVENDA' => 'float'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_RESTAURANTE',
		'ID_PRODUTO_RESTAURANTE',
		'ID_PRODUTO_RESTAURANTE_ADICIONAL',
		'PRECOVENDA',
		'OBS',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];

}
