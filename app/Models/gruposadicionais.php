<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ESTGRUPOADICIONAL
 *
 * @property int $ID
 * @property string $GRUPO
 * @property int $ID_PRODUTO
 * @property int $ID_GRUPO_RESTAURANTE
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @property \App\Models\ESTGRUPO $e_s_t_g_r_u_p_o
 * @property \App\Models\ESTGRUPORESTAURANTE $e_s_t_g_r_u_p_o_r_e_s_t_a_u_r_a_n_t_e
 * @property \App\Models\ESTPRODUTO $e_s_t_p_r_o_d_u_t_o
 *
 * @package App\Models
 */
class gruposadicionais extends Eloquent
{
	protected $table = 'EST_GRUPO_ADICIONAL';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PRODUTO' => 'int',
		'ID_GRUPO_RESTAURANTE' => 'int',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'GRUPO',
		'ID_PRODUTO',
		'ID_GRUPO_RESTAURANTE',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];

	public function e_s_t_g_r_u_p_o()
	{
		return $this->belongsTo(\App\Models\ESTGRUPO::class, 'GRUPO');
	}

	public function e_s_t_g_r_u_p_o_r_e_s_t_a_u_r_a_n_t_e()
	{
		return $this->belongsTo(\App\Models\ESTGRUPORESTAURANTE::class, 'ID_GRUPO_RESTAURANTE');
	}

	public function e_s_t_p_r_o_d_u_t_o()
	{
		return $this->belongsTo(\App\Models\ESTPRODUTO::class, 'ID_PRODUTO');
	}
}
