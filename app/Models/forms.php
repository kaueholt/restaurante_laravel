<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AMBFORM
 * 
 * @property int $ID
 * @property string $NOMEFORM
 * @property string $DESCRICAO
 * @property string $HELP
 *
 * @package App\Models
 */
class forms extends Eloquent
{
	protected $table = 'AMB_FORM';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'NOMEFORM',
		'DESCRICAO',
		'HELP'
	];
}
