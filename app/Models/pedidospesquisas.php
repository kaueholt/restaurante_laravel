<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FATPEDIDOPESQUISA
 *
 * @property int $ID
 * @property int $ID_PEDIDO
 * @property int $RESPOSTA1
 * @property int $RESPOSTA2
 * @property int $RESPOSTA3
 * @property int $PERGUNTA1
 * @property int $PERGUNTA2
 * @property int $PERGUNTA3
 * @property float $MEDIA
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @property \App\Models\FATPEDIDO $f_a_t_p_e_d_i_d_o
 *
 * @package App\Models
 */
class pedidospesquisas extends Eloquent
{
	protected $table = 'FAT_PEDIDO_PESQUISA';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PEDIDO' => 'int',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PEDIDO',
		'RESPOSTA1',
		'RESPOSTA2',
		'RESPOSTA3',
		'PERGUNTA1',
		'PERGUNTA2',
		'PERGUNTA3',
		'OBSERVACAO',
		'MEDIA',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];

	public function f_a_t_p_e_d_i_d_o()
	{
		return $this->belongsTo(\App\Models\FATPEDIDO::class, 'ID_PEDIDO');
	}
}
