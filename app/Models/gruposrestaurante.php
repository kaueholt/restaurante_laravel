<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ESTGRUPORESTAURANTE
 *
 * @property int $ID
 * @property string $GRUPO
 * @property int $ID_RESTAURANTE
 * @property string $ATIVO
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @property \App\Models\ESTGRUPO $e_s_t_g_r_u_p_o
 * @property \App\Models\CRMPESSOA $c_r_m_p_e_s_s_o_a
 * @property \Illuminate\Database\Eloquent\Collection $e_s_t__g_r_u_p_o__a_d_i_c_i_o_n_a_l_s
 *
 * @package App\Models
 */
class gruposrestaurante extends Eloquent
{
	protected $table = 'EST_GRUPO_RESTAURANTE';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_RESTAURANTE' => 'int',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'GRUPO',
		'ID_RESTAURANTE',
		'ATIVO',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];

	public function e_s_t_g_r_u_p_o()
	{
		return $this->belongsTo(\App\Models\ESTGRUPO::class, 'GRUPO');
	}

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(\App\Models\CRMPESSOA::class, 'ID_RESTAURANTE');
	}

	public function e_s_t__g_r_u_p_o__a_d_i_c_i_o_n_a_l_s()
	{
		return $this->hasMany(\App\Models\ESTGRUPOADICIONAL::class, 'ID_GRUPO_RESTAURANTE');
	}
}
