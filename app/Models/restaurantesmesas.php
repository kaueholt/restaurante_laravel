<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ESTRESTAURANTEMESA
 *
 * @property int $ID
 * @property int $ID_RESTAURANTE
 * @property string $DESCRICAO
 * @property string $LINKQRCODE
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 * @property boolean $CHAMA_GARCOM
 *
 * @property \App\Models\CRMPESSOA $c_r_m_p_e_s_s_o_a
 * @property \Illuminate\Database\Eloquent\Collection $f_a_t__p_e_d_i_d_o_s
 *
 * @package App\Models
 */
class restaurantesmesas extends Eloquent
{
	protected $table = 'EST_RESTAURANTE_MESA';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_RESTAURANTE' => 'int',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_RESTAURANTE',
		'DESCRICAO',
		'LINKQRCODE',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
        'USERUPDATE',
        'CHAMA_GARCOM'
	];

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(\App\Models\CRMPESSOA::class, 'ID_RESTAURANTE');
	}

	public function f_a_t__p_e_d_i_d_o_s()
	{
		return $this->hasMany(\App\Models\FATPEDIDO::class, 'ID_MESA');
	}
}
