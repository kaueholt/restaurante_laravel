<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;
/**
 * Class CRMPESSOA
 *
 * @property int $ID
 * @property string $NOME
 * @property string $NOMEREDUZIDO
 * @property string $PESSOA
 * @property string $AVATAR
 * @property string $CUSTOMER_TOKEN_IUGU
 * @property string $CNPJCPF
 * @property string $INSCESTRG
 * @property string $INSCMUNICIPAL
 * @property int $CEP
 * @property string $ENDERECO
 * @property string $NUMERO
 * @property string $COMPLEMENTO
 * @property string $BAIRRO
 * @property string $CIDADE
 * @property string $UF
 * @property string $FONE
 * @property string $CELULAR
 * @property string $EMAIL
 * @property string $HOMEPAGE
 * @property string $CLASSE
 * @property string $ATIVIDADE
 * @property string $USERLOGIN
 * @property string $PASSWORD
 * @property string $CUSTOMER_TOKEN_IUGU
 * @property string $OBS
 * @property string $UID
 * @property string $UIDPHONE
 * @property float $LATITUDE
 * @property float $LONGITUDE
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 * @property string $INICIOMANHA
 * @property string $FIMMANHA
 * @property string $INICIOTARDE
 * @property string $FIMTARDE
 * @property string $INICIOSABADOMANHA
 * @property string $INICIOSABADOTARDE
 * @property string $FIMSABADOMANHA
 * @property string $FIMSABADOTARDE
 * @property string $INICIODOMINGOMANHA
 * @property string $INICIODOMINGOTARDE
 * @property string $FIMDOMINGOMANHA
 * @property string $FIMDOMINGOTARDE
 * @property string $DIASEMANA
 * @property \App\Models\FINATIVIDADE $f_i_n_a_t_i_v_i_d_a_d_e
 * @property \App\Models\FINCLASSE $f_i_n_c_l_a_s_s_e
 * @property \Illuminate\Database\Eloquent\Collection $e_s_t__g_r_u_p_o__r_e_s_t_a_u_r_a_n_t_e_s
 * @property \Illuminate\Database\Eloquent\Collection $e_s_t__p_r_o_d_u_t_o__r_e_s_t_a_u_r_a_n_t_e_s
 * @property \Illuminate\Database\Eloquent\Collection $e_s_t__r_e_s_t_a_u_r_a_n_t_e__m_e_s_a_s
 * @property \Illuminate\Database\Eloquent\Collection $f_a_t__c_u_p_o_m__p_e_s_s_o_a_s
 * @property \Illuminate\Database\Eloquent\Collection $f_a_t__p_e_d_i_d_o_s
 *
 * @package App\Models
 */
class pessoas extends Authenticatable implements JWTSubject
{
	use Notifiable;
	
	protected $table = 'CRM_PESSOA';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'CEP' => 'int',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int',
		'LATITUDE' => 'float',
		'LONGITUDE' => 'float',
		'INICIOMANHA' => 'string',
		'FIMMANHA' => 'string',
		'INICIOTARDE' => 'string',
		'FIMTARDE' => 'string',
		'INICIOSABADOMANHA' => 'string',
		'INICIOSABADOTARDE' => 'string',
		'FIMSABADOMANHA' => 'string',
		'FIMSABADOTARDE' => 'string',
		'INICIODOMINGOMANHA' => 'string',
		'INICIODOMINGOTARDE' => 'string',
		'FIMDOMINGOMANHA' => 'string',
		'FIMDOMINGOTARDE' => 'string',
		'DIASEMANA' => 'string',
	];

	protected $dates = [
		'DATAANASC',
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $hidden = [
        'PASSWORD', 'remember_token',
    ];

	protected $fillable = [
		'NOME',
		'NOMEREDUZIDO',
		'PESSOA',
		'AVATAR',
		'CUSTOMER_TOKEN_IUGU',
		'CNPJCPF',
		'INSCESTRG',
		'INSCMUNICIPAL',
		'CEP',
		'ENDERECO',
		'NUMERO',
		'COMPLEMENTO',
		'BAIRRO',
		'CIDADE',
		'UF',
		'FONE',
		'CELULAR',
		'EMAIL',
		'HOMEPAGE',
		'CLASSE',
		'ATIVIDADE',
		'USERLOGIN',
		'PASSWORD',
		'OBS',
		'DATAANASC',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE',
        'UID',
        'UIDPHONE',
        'LATITUDE',
        'LONGITUDE',
        'INICIOMANHA',
        'FIMMANHA',
        'INICIOTARDE',
        'FIMTARDE',
        'INICIOSABADOMANHA',
	    'INICIOSABADOTARDE',
	    'FIMSABADOMANHA',
	    'FIMSABADOTARDE',
	    'INICIODOMINGOMANHA',
	    'INICIODOMINGOTARDE',
	    'FIMDOMINGOMANHA',
        'FIMDOMINGOTARDE',
        'DIASEMANA'
	];

	public function f_i_n_a_t_i_v_i_d_a_d_e()
	{
		return $this->belongsTo(\App\Models\FINATIVIDADE::class, 'ATIVIDADE');
	}

	public function f_i_n_c_l_a_s_s_e()
	{
		return $this->belongsTo(\App\Models\FINCLASSE::class, 'CLASSE');
	}

	public function e_s_t__g_r_u_p_o__r_e_s_t_a_u_r_a_n_t_e_s()
	{
		return $this->hasMany(\App\Models\ESTGRUPORESTAURANTE::class, 'ID_RESTAURANTE');
	}

	public function e_s_t__p_r_o_d_u_t_o__r_e_s_t_a_u_r_a_n_t_e_s()
	{
		return $this->hasMany(\App\Models\ESTPRODUTORESTAURANTE::class, 'ID_RESTAURANTE');
	}

	public function e_s_t__r_e_s_t_a_u_r_a_n_t_e__m_e_s_a_s()
	{
		return $this->hasMany(\App\Models\ESTRESTAURANTEMESA::class, 'ID_RESTAURANTE');
	}

	public function f_a_t__c_u_p_o_m__p_e_s_s_o_a_s()
	{
		return $this->hasMany(\App\Models\FATCUPOMPESSOA::class, 'ID_PESSOA');
	}

	public function f_a_t__p_e_d_i_d_o_s()
	{
		return $this->hasMany(\App\Models\FATPEDIDO::class, 'ID_CLIENTE');
	}

	public function getJWTIdentifier(){
        return $this->getKey();
    }

    public function getJWTCustomClaims(){
        return [];
    }
}
