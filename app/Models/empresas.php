<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AMBEMPRESA
 * 
 * @property int $EMPRESA
 * @property string $NOMEEMPRESA
 * @property string $NOMEFANTASIA
 * @property string $TIPOEND
 * @property string $TITULOEND
 * @property string $ENDERECO
 * @property string $NUMERO
 * @property string $COMPLEMENTO
 * @property string $BAIRRO
 * @property string $CIDADE
 * @property string $UF
 * @property int $CEP
 * @property string $CNPJ
 * @property string $INSCESTADUAL
 * @property string $INSCMUNICIPAL
 * @property string $FONEAREA
 * @property string $FONENUM
 * @property string $FAXAREA
 * @property string $FAXNUM
 * @property \Carbon\Carbon $INICIOMANHA
 * @property \Carbon\Carbon $FIMMANHA
 * @property \Carbon\Carbon $INICIOTARDE
 * @property \Carbon\Carbon $FIMTARDE
 * @property \Carbon\Carbon $INICIOSABADO
 * @property \Carbon\Carbon $FIMSABADO
 * @property string $FAX2AREA
 * @property string $FAX2NUM
 * @property string $EMAIL
 * @property int $CENTROCUSTO
 * @property int $CLIENTE
 * @property int $FORNECEDOR
 * @property boolean $LOGO
 * @property string $NOMECONTATO
 * @property int $CONTA
 * @property string $CODMUNICIPIO
 * @property string $REGIMETRIBUTARIO
 *
 * @package App\Models
 */
class empresas extends Eloquent
{
	protected $table = 'AMB_EMPRESA';
	protected $primaryKey = 'EMPRESA';
	public $timestamps = false;

	protected $casts = [
		'CEP' => 'int',
		'CENTROCUSTO' => 'int',
		'CLIENTE' => 'int',
		'FORNECEDOR' => 'int',
		'LOGO' => 'boolean',
		'CONTA' => 'int'
	];

	// protected $dates = [
	// 	'INICIOMANHA',
	// 	'FIMMANHA',
	// 	'INICIOTARDE',
	// 	'FIMTARDE',
	// 	'INICIOSABADO',
	// 	'FIMSABADO'
	// ];

	protected $fillable = [
		'NOMEEMPRESA',
		'NOMEFANTASIA',
		'TIPOEND',
		'TITULOEND',
		'ENDERECO',
		'NUMERO',
		'COMPLEMENTO',
		'BAIRRO',
		'CIDADE',
		'UF',
		'CEP',
		'CNPJ',
		'INSCESTADUAL',
		'INSCMUNICIPAL',
		'FONEAREA',
		'FONENUM',
		'FAXAREA',
		'FAXNUM',
		'INICIOMANHA',
		'FIMMANHA',
		'INICIOTARDE',
		'FIMTARDE',
		'INICIOSABADO',
		'FIMSABADO',
		'FAX2AREA',
		'FAX2NUM',
		'EMAIL',
		'CENTROCUSTO',
		'CLIENTE',
		'FORNECEDOR',
		'LOGO',
		'NOMECONTATO',
		'CONTA',
		'CODMUNICIPIO',
		'REGIMETRIBUTARIO'
	];
}
