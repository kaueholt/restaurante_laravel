<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FATPEDIDOITEMADICIONAL
 *
 * @property int $ID
 * @property int $ID_PEDIDO_ITEM
 * @property int $ID_PRODUTO
 * @property float $VALOR
 * @property float $QUANTIDADE
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @property \App\Models\ESTPRODUTO $e_s_t_p_r_o_d_u_t_o
 * @property \App\Models\FATPEDIDOITEM $f_a_t_p_e_d_i_d_o_i_t_e_m
 *
 * @package App\Models
 */
class pedidositensadicionais extends Eloquent
{
	protected $table = 'FAT_PEDIDO_ITEM_ADICIONAL';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PEDIDO_ITEM' => 'int',
		'ID_PRODUTO' => 'int',
		'VALOR' => 'float',
		'QUANTIDADE' => 'float',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PEDIDO_ITEM',
		'ID_PRODUTO',
		'VALOR',
		'QUANTIDADE',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];

	public function e_s_t_p_r_o_d_u_t_o()
	{
		return $this->belongsTo(\App\Models\ESTPRODUTO::class, 'ID_PRODUTO');
	}

	public function f_a_t_p_e_d_i_d_o_i_t_e_m()
	{
		return $this->belongsTo(\App\Models\FATPEDIDOITEM::class, 'ID_PEDIDO_ITEM');
	}
}
