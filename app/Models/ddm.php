<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AMBDDM
 * 
 * @property int $ID
 * @property string $ALIAS
 * @property int $NRCAMPO
 * @property string $CAMPO
 * @property string $TIPO
 * @property int $TAMANHO
 * @property int $DECIMAIS
 * @property string $FORMATO
 * @property string $REQUERIDO
 * @property string $PADRAO
 * @property string $LEGENDA
 * @property string $MASCARA
 * @property string $NOMEEXTERNO
 * @property \Carbon\Carbon $ALTERACAO
 * @property string $DESCRICAO
 * @property string $SQL
 * @property string $MENSAGEM
 * @property string $MENU
 *
 * @package App\Models
 */
class ddm extends Eloquent
{
	protected $table = 'AMB_DDM';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'NRCAMPO' => 'int',
		'TAMANHO' => 'int',
		'DECIMAIS' => 'int'
	];

	protected $dates = [
		'ALTERACAO'
	];

	protected $fillable = [
		'ALIAS',
		'NRCAMPO',
		'CAMPO',
		'TIPO',
		'TAMANHO',
		'DECIMAIS',
		'FORMATO',
		'REQUERIDO',
		'PADRAO',
		'LEGENDA',
		'MASCARA',
		'NOMEEXTERNO',
		'ALTERACAO',
		'DESCRICAO',
		'SQL',
		'MENSAGEM',
		'MENU'
	];
}
