<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AMBUSUARIO
 *
 * @property int $USUARIO
 * @property string $NOME
 * @property string $LOGIN
 * @property string $NOMEREDUZIDO
 * @property int $ID_PESSOA
 * @property string $SENHA
 * @property string $SITUACAO
 * @property string $TIPO
 * @property int $GRUPO
 * @property string $SMTP
 * @property string $EMAIL
 * @property string $EMAILSENHA
 *
 * @property \Illuminate\Database\Eloquent\Collection $a_m_b__u_s_u_a_r_i_o__a_u_t_o_r_i_z_a_s
 *
 * @package App\Models
 */
class usuarios extends Eloquent
{
	protected $table = 'AMB_USUARIO';
	public $primaryKey = 'USUARIO';
	public $timestamps = false;

	protected $casts = [
		'ID_PESSOA' => 'int',
		'GRUPO' => 'int'
	];

	protected $fillable = [
		'NOME',
		'LOGIN',
		'NOMEREDUZIDO',
		'ID_PESSOA',
		'SENHA',
		'SITUACAO',
		'TIPO',
		'GRUPO',
		'SMTP',
		'EMAIL',
		'EMAILSENHA'
	];

	public function a_m_b__u_s_u_a_r_i_o__a_u_t_o_r_i_z_a_s()
	{
		return $this->hasMany(\App\Models\AMBUSUARIOAUTORIZA::class, 'USUARIO');
	}
}
