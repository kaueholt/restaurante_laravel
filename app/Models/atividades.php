<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FINATIVIDADE
 *
 * @property string $ATIVIDADE
 * @property string $DESCRICAO
 * @property string $TIPO
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @property \Illuminate\Database\Eloquent\Collection $c_r_m__p_e_s_s_o_a_s
 *
 * @package App\Models
 */
class atividades extends Eloquent
{
	protected $table = 'FIN_ATIVIDADE';
	protected $primaryKey = 'ATIVIDADE';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
        'ATIVIDADE',
		'DESCRICAO',
		'TIPO',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];

	public function c_r_m__p_e_s_s_o_a_s()
	{
		return $this->hasMany(\App\Models\CRMPESSOA::class, 'ATIVIDADE');
	}
}
