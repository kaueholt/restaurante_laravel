<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FATCUPOMPESSOA
 *
 * @property int $ID
 * @property int $ID_CUPOM
 * @property int $ID_PESSOA
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @property \App\Models\CRMPESSOA $c_r_m_p_e_s_s_o_a
 * @property \App\Models\FATCUPOM $f_a_t_c_u_p_o_m
 * @property \Illuminate\Database\Eloquent\Collection $f_a_t__p_e_d_i_d_o_s
 *
 * @package App\Models
 */
class cupompessoas extends Eloquent
{
	protected $table = 'FAT_CUPOM_PESSOA';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_CUPOM' => 'int',
		'ID_PESSOA' => 'int',
		'ID_PEDIDO' => 'int',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int',
		'UTILIZADO' => 'boolean'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_CUPOM',
		'ID_PESSOA',
		'ID_PEDIDO',
		'STATUS',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
        'USERUPDATE',
        'UTILIZADO'
	];

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(\App\Models\CRMPESSOA::class, 'ID_PESSOA');
	}

	public function f_a_t_c_u_p_o_m()
	{
		return $this->belongsTo(\App\Models\FATCUPOM::class, 'ID_CUPOM');
	}

	public function f_a_t__p_e_d_i_d_o_s()
	{
		return $this->hasMany(\App\Models\FATPEDIDO::class, 'ID_CUPOM_PESSOA');
	}
}
