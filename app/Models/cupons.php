<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Nov 2019 07:43:00 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FATCUPOM
 * 
 * @property int $ID
 * @property string $DESCRICAO
 * @property float $VALOR
 * @property \Carbon\Carbon $VALIDADEINICIO
 * @property \Carbon\Carbon $VALIDADEFIM
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 * 
 * @property \Illuminate\Database\Eloquent\Collection $f_a_t__c_u_p_o_m__p_e_s_s_o_a_s
 *
 * @package App\Models
 */
class cupons extends Eloquent
{
	protected $table = 'FAT_CUPOM';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'VALOR' => 'float',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'VALIDADEINICIO',
		'VALIDADEFIM',
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'DESCRICAO',
		'VALOR',
		'VALIDADEINICIO',
		'VALIDADEFIM',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];

	public function f_a_t__c_u_p_o_m__p_e_s_s_o_a_s()
	{
		return $this->hasMany(\App\Models\FATCUPOMPESSOA::class, 'ID_CUPOM');
	}
}
