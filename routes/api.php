<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/**
 * @SWG\Swagger(
 *     basePath="/api/v1",
 *     schemes={"http", "https"},
 *     host=L5_SWAGGER_CONST_HOST,
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="L5 Swagger API",
 *         description="Mizefood API",
 *         @SWG\Contact(
 *             email="marcio.sergio@admsistema.com.br"
 *         ),
 *     )
 * )
 */
Route::group(array('prefix' => 'v1'), function()
{

    Route::get('/', function () {
        return response()->json(['message' => 'Mizefood API', 'status' => 'Connected']);
    });

    Route::resource('pessoas', 'PessoasController');
    //Route::resource('auth', 'UserAuthController');

    Route::get('/uid/{UID}', 'UserAuthController@UID');
    Route::get('/email/{email}', 'UserAuthController@email');
    Route::post('/login', 'UserAuthController@Login');
    Route::get('/auth/uid/{UID}', 'Api\Auth\LoginController@UID');
    Route::post('/auth/login', 'Api\Auth\LoginController@login');
    Route::get('/auth/refresh', 'Api\Auth\LoginController@refresh');
    Route::post('/loginGarcom', 'UserAuthController@LoginGarcom');
    
    Route::get('/teste/{pessoa}/cidade/{cidade}', 'Api\PessoaController@showByCidade');
    
    // Rotas default
    Route::get('/default/{table}', 'DefaultController@index');
    Route::get('/default/{table}/{id}', 'DefaultController@show');
    Route::post('/default/{table}', 'DefaultController@store');
    Route::put('/default/{table}/{id}', 'DefaultController@update');
    Route::delete('/default/{table}/{id}', 'DefaultController@destroy');

    // Rotas especificas
    //Pessoas
    Route::get('/pessoas/{pessoa}/cidade/{cidade}', 'PessoasController@showByCidade');
    Route::get('/pessoas/{pessoa}/cidade/{cidade}/{nomeRestaurante}', 'PessoasController@showByCidade');
    Route::get('/geoloc/{latidude}/{longitude}/{cidade}', 'PessoasController@showByCoordinates');
    Route::get('/geoloc/{latidude}/{longitude}/{cidade}/{sortByRating}', 'PessoasController@showByCoordinates');
    Route::get('/geoloc/{latitude}/{longitude}/{cidade}/{sortByRating}/{nomeRestaurante}', 'PessoasController@showByCoordinates');
    Route::get('/pessoas/telefone/{telefone}', 'PessoasController@showByTelefone');
    Route::get('/pessoas/cidade/bairro/{cidade}/{bairro}', 'PessoasController@showCidadeBairro');
    Route::get('/pessoas/cupons/{id}', 'PessoasController@showCupons');

    //Pedidos
    Route::get('/pedidos/{id}', 'PedidosController@showPedidosItensEAdicionaisEAvaliacao');
    Route::get('/pedidos/usuarios/{id}', 'PedidosController@showPedidosUsuarios');
    Route::get('/pedidos/usuarios/{id}/{situacao}', 'PedidosController@showPedidosUsuarios');
    Route::get('/pedidos/itens/{id}', 'PedidosController@showPedidosItens');
    Route::get('/pedidos/itens/status/{id}/{status}', 'PedidosController@showPedidosItensStatus');
    Route::post('/pedidos/itens', 'PedidosController@storePedidosItens');
    Route::post('/pedidos/itens/adicionais', 'PedidosController@storePedidosItensAdicionais');
    Route::post('/pedidos/satisfacao', 'PedidosController@storeAvaliacaoPedido');
    Route::get('/pedidos/satisfacao/{id}', 'PedidosController@showAvaliacaoPedido');
    Route::get('/pedidos/satisfacao/cliente/{id}', 'PedidosController@showAvaliacoesPedidosCliente');
    Route::post('/pedidos/{id}/pagamento', 'PagamentosController@cobrancaDireta');

    //Restaurantes
    Route::get('/restaurante/{id}', 'RestaurantesController@showRestaurante');
    Route::get('/restaurantes/{id}', 'RestaurantesController@showRestauranteMesas');
    Route::get('/restaurantes/{id}/mesa/{idMesa}', 'RestaurantesController@showPedidoMesa');
    Route::get('/restaurantes/cupons/{id}', 'RestaurantesController@showCuponsRestaurante');
    Route::get('/restaurantes/cupons/disponiveis/{id}', 'RestaurantesController@showCuponsDisponiveis');
    Route::put('/restaurantes/garcom', 'RestaurantesController@toogleChamaGarcom');
    Route::get('/restaurantes/cidade/{cidade}', 'RestaurantesController@showByCidade');

    //Produtos
    Route::get('/produtos/promocoes/geoloc/{latitude}/{longitude}', 'ProdutosController@showProdutosPromocoesGeolocCidade');
    Route::get('/produtos/promocoes/geoloc/{latitude}/{longitude}/{cidade}', 'ProdutosController@showProdutosPromocoesGeolocCidade');
    Route::get('/produtos/promocoes/{pessoa}/cidade/{cidade}', 'ProdutosController@showProdutosPromocoesCidade');
    Route::get('/produtos/promocoes/{pessoa}/{id}', 'ProdutosController@showProdutosPromocoes');
    Route::get('/produtos/maispedidos/{id}', 'ProdutosController@showProdutosMaisPedidos');
    Route::get('/produtos/pesquisa/{pessoa}/{cidade}/{descricao}', 'ProdutosController@pesquisaProdutos');
    Route::get('/produtos/descricao/{id}/{descricao}', 'ProdutosController@showProdutosDescricao');
    Route::get('/produtos/cardapio/{id}/{tipo}', 'ProdutosController@showProdutosCardapio');
    Route::get('/produtos/cardapio/{id}/{tipo}/{idProduto}', 'ProdutosController@showProdutosCardapio');
    Route::get('/produtos/imagens/{id}', 'ProdutosController@showImagensProduto');
    Route::get('/produtos/imagem/{id}', 'ProdutosController@showImagemDestaqueProduto');
    Route::get('/produtos/imagem/{id}/{idRestaurante}', 'ProdutosController@showImagemDestaqueProdutoRestaurante');

});

Route::get('/', function () {
    return redirect('api');
});
